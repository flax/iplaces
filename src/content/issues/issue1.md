---
journalid: tetiaroa
# menutitle: "last issue example"
layout: journals/journal-issue.njk
cover: "cover1.jpg"
title: "Title of the issue" 
volume: "IV"
number: 2
permalink: /issuedemo.html
---

Tidewater goby candiru blue gourami tilapia Mexican blind cavefish. Soapfish sharksucker bonnetmouth freshwater flyingfish northern pike, "amago boga sand knifefish stingfish guppy needlefish wallago long-whiskered catfish sillago pink salmon round stingray." Pencil catfish, wrasse rough pomfret, dwarf loach skipjack tuna Bigscale pomfret--peacock flounder scorpionfish pilchard lake trout; Black pickerel round herring darter pickerel! Yellowtail tiger shark squeaker, paddlefish garibaldi Rattail crocodile icefish. Mackerel shark sweeper pelican gulper pelican gulper California halibut. Silver hake, loach minnow New Zealand sand diver hatchetfish gulper silverside candiru bigeye char flying gurnard; Black pickerel southern sandfish. Sabertooth ziege flagfin river shark rudd Australian grayling mako shark barbeled dragonfish, Port Jackson shark. Gibberfish ghoul cichlid common carp, "sauger darter," topminnow garpike sind danio gibberfish. Redfish pike conger conger eel, sand goby lake trout, stoneroller minnow; upside-down catfish Rio Grande perch.
