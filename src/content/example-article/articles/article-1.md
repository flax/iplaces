---
journalid: tetiaroa-ecostation
journaltitle: Tetiaroa Ecostation
# menutitle: article example
layout: journals/journal-article-single.njk
permalink: "/{{journaltitle | slugify}}/article-view/"
thumbnail: "article1-thumb.jpg"
article:
  dates:
    published: 11-9-2023
  issue:
    cover: cover1.jpg
    title: Title of the issue
    volume: "Volume IV, issue 2"
    number: 2
  title: ARMS for Biodiversity Baselines in Polynesie Francaise
  author:
    - given: Christopher
      family: Meyer
      affiliation:
        - Smithsonian Institution
    - given: David
      family: Liittschwager
      affiliation:
    - given: Emily
      family: Schmeltzer
      affiliation:
        - National Geographic Society
    - given: Jessica
      family: Goodheart Ryan Lauter
      affiliation:
        - California State University (CSU), Moss Landing Marine Laboratories
    - given: Zachariah
      family: Kobrinsky
      affiliation:
        - University of Maryland
  licence: https://creativecommons.org/licenses/by/4.0/
  abstract: |
    Fish and coral species comprise less than 1% of all coral reef biodiversity yet almost all assessments of reef status are made exclusively on those groups. Autonomous Reef Monitoring Structures (ARMS) are standardized sampling arrays meant to mimic the complexity of the reef and capture the other 99% of diversity. Building off the success of the Moorea Biocode Project, ARMS provide a more comprehensive baseline across taxonomic and functional domains for documenting reef diversity and monitoring changing patterns through future ocean conditions.

    ARMS sample biodiversity over precisely the same surface area in the exact same manner. Like pre-fabricated housing, ARMS are deployed on the ocean bottom for one year and then collected to see what species have moved in. The integration of traditional taxonomic methods with DNA barcoding and next-generation metagenomic approaches vastly increases the number of species included in biodiversity measurements. The standardized approach allows comparison from site to site or over time. Over 1000 ARMS have been deployed at over 100 sites across the worlds oceans.

    ARMS were deployed for the first time at two sites on Tetiaroa in the spring of 2014 and will be recovered in June 2015. The collected communities will be processed to establish the first ever biodiversity profile for Tetiaroas reefs. The inventory generated from analyses of these ARMS will not only provide a biodiversity baseline for Tetiaroa, but also result in the first ever comparison of reef communities between high islands (Moorea) and an atoll (Tetiaroa) using this more expansive approach.

  citation: |
    Meyer, C., Liittschwager, D., Schmeltzer, E., Goodheart, J., Lauter, R., & Kobrinsky, Z. (2015).
    ARMS for Biodiversity Baselines in Polynesie Francaise. Tetiaroa Society. [https://doi.org/10.17913/F3/100501.31069](https://doi.org/10.17913/F3/100501.31069)

  keywords:
    - Electroconductive polymere
    - polyaniline
    - polypyrrole
    - polythiophene
    - polyphenylene
    - polyacetylene

  content: |
    <h2>Introduction</h2>
    <p class="paragraph">
    The majority of polymers are insulators due to the strong covalent
    bonding between atoms in their chains, which results in the absence of
    free-moving electrons or ions [1, 2]. However, for medical implants in
    cardiac, nerve, and skeletal muscle tissues, this lack of electrical
    conductivity presents a significant drawback [3]. To achieve a fully
    functional restoration, medical implants in these tissues need to
    exhibit a certain level of electrical conductivity.
    </p>
    <p class="paragraph">
    Electroconductive polymers (ECPs) are organic materials that possess
    unique electrical and optical properties, similar to those of inorganic
    semiconductors and metals [3, 4]. They can be synthesized using
    cost-effective approaches and can be assembled into supramolecular
    structures with multifunctional capabilities. ECPs have demonstrated
    promising capabilities to induce various cellular mechanisms, which
    broadens their unique applications in the biomedical field [4]. They are
    attractive for various biomedical applications due to their intelligent
    response to electrical fields from different types of tissues. ECPs have
    been used to enhance the electrical sensitivity, speed, and stability of
    various biomedical devices and their interfaces with biological tissues.
    </p>
    <p class="paragraph">
    There are five common types of ECPs: polyaniline, polypyrrole,
    polythiophene, polyphenylene, and polyacetylene. The electrical
    conductivity (S/cm) of these ECPs and their abbreviations are shown in
    <strong>Table 1</strong>. In addition to these polymers, conductive
    composite polymers, such as π-conjugated ECPs, conductive nanoparticles,
    carbon-based nanoparticles, and alloys, have also been extensively
    studied [5].
    </p>
    <p class="paragraph">
    ECPs have several potential applications, including electrically induced
    targeted drug release systems, biosensors, and metabolic markers of
    stress [6]. Moreover, the clinical applications of ECPs are vast,
    including the development of nerve conduits, synthetic scaffolds that
    support the successful regeneration of myocardial tissue, and deep brain
    stimulation for the treatment of neurological disorders such as
    Parkinson's disease. There are still many potential uses of ECPs, from
    biosensors and bioactuators to drug delivery systems and neural
    prosthetics [7-9].
    </p>
    <p class="paragraph">
    This manuscript presents an in-depth study of the structure and
    properties of ECPs, including their electrical and optical properties,
    which directly influence their performance in biomedical applications.
    The manuscript not only discusses the potential applications of ECPs,
    such as in tissue engineering, regenerative medicine, biosensors, and
    neural prosthetics, but also the challenges in their development,
    including the need for biocompatibility, stability, and enhanced
    mechanical properties. Furthermore, the review provides novel insights
    into the methods for modifying ECPs to enhance their biocompatibility
    and reduce toxicity. It discusses innovative approaches such as surface
    modification, polymer blending, and preconditioning techniques. The
    manuscript also brings to light the key advantages of ECPs in the
    biomedical field, such as cost-effectiveness, low detection limit for
    biosensors, chemical stability, good processability, tunable electrical
    properties, and stimuli responsiveness. In essence, this manuscript
    presents a novel, well-rounded perspective on the use and potential of
    ECPs in the biomedical field, making it a valuable resource for
    researchers and industry professionals alike.
    </p>
    <h3>
    <strong>Table 1</strong>. Conductivities of common types of
    electroconductive polymers [3]
    </h3>
    <table>
    <tbody>
      <tr>
        <td>
          <p class="paragraph">
            <strong>Electroconductive polymers</strong>
          </p>
        </td>
        <td>
          <p class="paragraph"><strong>Abbreviation</strong></p>
        </td>
        <td>
          <p class="paragraph"><strong>Formula</strong></p>
        </td>
        <td>
          <p class="paragraph"><strong>Conductivity (S/cm)</strong></p>
        </td>
      </tr>
      <tr>
        <td><p class="paragraph">Polyaniline</p></td>
        <td><p class="paragraph">PANI</p></td>
        <td>
          <p class="paragraph">
            [C<sub>6</sub>H<sub>4</sub>NH]<sub>n</sub>
          </p>
        </td>
        <td>
          <p class="paragraph">10<sup>-2</sup>-10<sup>0</sup></p>
        </td>
      </tr>
      <tr>
        <td><p class="paragraph">Polypyrrole</p></td>
        <td><p class="paragraph">PPy</p></td>
        <td>
          <p class="paragraph">
            [C<sub>4</sub>H<sub>2</sub>NH]<sub>n</sub>
          </p>
        </td>
        <td><p class="paragraph">2-100</p></td>
      </tr>
      <tr>
        <td><p class="paragraph">Polythiophene</p></td>
        <td><p class="paragraph">PT</p></td>
        <td>
          <p class="paragraph">[C<sub>4</sub>H<sub>4</sub>S]<sub>n</sub></p>
        </td>
        <td>
          <p class="paragraph">10<sup>0</sup>-10<sup>3</sup></p>
        </td>
      </tr>
      <tr>
        <td><p class="paragraph">Poly(p-phenylene)</p></td>
        <td><p class="paragraph">PPP</p></td>
        <td>
          <p class="paragraph">[C<sub>6</sub>H<sub>4</sub>]<sub>n</sub></p>
        </td>
        <td>
          <p class="paragraph">10<sup>−3</sup> −10<sup>2</sup></p>
        </td>
      </tr>
      <tr>
        <td><p class="paragraph">Polyacetylene</p></td>
        <td><p class="paragraph">PA</p></td>
        <td>
          <p class="paragraph">[C<sub>2</sub>H<sub>2</sub>]<sub>n</sub></p>
        </td>
        <td>
          <p class="paragraph">10<sup>5</sup></p>
        </td>
      </tr>
    </tbody>
    </table>
    <h2>Common types of ECPs</h2>
    <h3>Polyaniline (PANI)</h3>
    <p class="paragraph">
    Polyaniline (PANI) is a highly regarded ECP due to its easy preparation,
    cost-effectiveness, high electrical conductivity, biocompatibility, low
    toxicity, and environmental stability [10]. However, it has certain
    drawbacks, such as low solubility, insolubility in common solvents,
    infusibility, weak processability, and decreasing electrical
    conductivity over time [11]. To address these limitations, various
    techniques have been developed, including re-doping with functionalized
    organic acids, copolymerization with PANI derivatives or other polymers,
    and preparing blends and nanocomposites with various materials [12].
    </p>
    <p class="paragraph">
    PANI nanocomposites are highly promising ECP nanocomposites as they
    combine the PANI matrix with conducting or insulating nanofillers to
    enhance their properties. These nanocomposites have improved properties,
    making them valuable in various fields such as electronics, water
    purification, tissue regeneration, and antimicrobial therapy.
    </p>
    <p class="paragraph">
    PANI or "aniline black" is one of the oldest ECPs, discovered in the
    mid-19<sup>th</sup> century. PANI molecular structure may possess
    benzenoid, quinonoid units, or both types in different proportions [13].
    Chemical and electrochemical oxidative polymerization are the most
    common methods for synthesizing PANIs in an acidic medium. Ammonium
    persulfate and potassium persulfate are the most widely used initiators
    for chemical polymerization. The electrochemical method is preferred for
    small-scale synthesis, while the chemical method allows large-scale
    polymer and nanocomposite preparation [14]. The electrode coating and
    co-deposition approaches are two electrochemical methods used for PANI
    synthesis. PANIs of various nanostructures with different properties
    have been synthesized using several techniques, such as solution,
    self-assembling, heterophase interfacial, and electrochemical
    polymerizations. PANI nanostructures, such as nanospheres, nanogranules,
    nanorods, nanoflowers, nanofibers, and nanotubes, have been synthesized
    using different procedures and parameters, including the initiator or
    oxidant, pH, temperature, solvent, chemical additives, chemical
    oxidation process, template, electrochemistry, radiochemistry, and
    sonochemistry. The electrical properties of PANI-based polymers depend
    on their microscopic and macroscopic properties.
    </p>
    <p class="paragraph">
    PANI is chemically stable and structurally resistant in acidic and
    alkaline solutions, without any degradation or chemical reaction.
    Depending on the redox states, PANI has different solubility in common
    organic solvents. PANI pellets demonstrate superior mechanical strength
    due to the good compactness of the PANI powders. The physical and
    mechanical properties of PANI nanocomposites, such as Young's modulus
    and heat resistance, are significantly improved.
    <strong>Figure 1</strong> displays the various oxidation forms of PANI.
    PANI has shown promise in the field of biomedicine. It possesses
    biocompatibility and can interact with biological systems, making it
    suitable for applications such as drug delivery, tissue engineering
    scaffolds, and biosensors. Polyaniline-based materials have been
    explored for their potential in controlled drug release, cell culture
    substrates, and bioelectrodes, among others [4].
    </p>
    <p class="paragraph">
    However, challenges remain in optimizing and expanding the applications
    of PANI. These include improving its stability under prolonged exposure
    to environmental factors, enhancing its processability for large-scale
    production, and addressing potential issues related to its long-term
    performance and compatibility in specific applications.
    </p>
    <p class="paragraph">
    In summary, PANI is a versatile conducting polymer with excellent
    electrical conductivity, environmental stability, and tunable
    properties. Its broad range of applications spans from electronics and
    energy storage to corrosion protection, biomedical devices, and beyond.
    Ongoing research and development efforts continue to explore and unlock
    the full potential of polyaniline in various fields, driving
    advancements in materials science and technology.
    </p>
    <p class="paragraph"></p>
    <p class="paragraph"></p>
    <figure>
    <img class="fake"
      src="/images/image005.png"
      alt="image005.png"
      data-id="186274ef-765b-48fd-8228-2d9749cdc685"
      data-fileid="cd774ecd-b97e-42e8-ab8c-780adb8384eb"
    />
    <figcaption id="186274ef-765b-48fd-8228-2d9749cdc685" class="decoration">
      <strong>Figure 1</strong>. Different oxidation forms of polyaniline.
      Reprinted with permission from [4]
    </figcaption>
    </figure>
    <p class="paragraph"></p>
    <p class="paragraph"></p>
    <p class="paragraph"></p>
    <p class="paragraph"></p>
    <h2>Polypyrrole (PPy)</h2>
    <p class="paragraph">
    Polypyrrole (PPy) is a type of polymer that has gained attention for its
    excellent electrical conductivity, which makes it a promising candidate
    for electronic applications. The polymer's high conductivity is
    primarily attributed to the polymer backbones that facilitate the easy
    movement of electrons within and between them [15]. PPy has a conjugated
    backbone with alternating single and double bonds with
    sp<sup>2</sup>-hybridized carbon atoms. The electrical conductivity of
    PPy changes from an insulator to a semiconductor, with a band gap &lt;
    2.5 eV, once it is doped [16].
    </p>
    <p class="paragraph">
    &nbsp;The doping process creates polarons and bipolaron species that
    enhance the electrical conductivity of PPy. The exact mechanism of
    charge transfer in a PPy chain has not been fully elucidated, but it is
    believed that polarons and bipolarons are created during the doping
    process [17].
    </p>
    <p class="paragraph">
    Polarons are π-conjugated structures based on aromatic rings that are
    formed from the elimination of an electron from the p system of PPy,
    generating a free radical and a cation [18]. The radical cation then
    undergoes a bond rearrangement to form a quinonoid system. In general,
    species with two charges in the same unit cell are termed a bipolaron,
    regardless of whether the species are stable or not. Doping may be
    performed chemically, electrochemically, or by using photo-doping, and
    organic and inorganic dopants have been used for doping PPy [19]. Small
    and large molecule dopants modulate the electrical conductivity and the
    surface structural properties of PPy in different fashions, with larger
    dopants enhancing the electrochemical stability of the synthesized PPy.
    </p>
    <p class="paragraph">
    Apart from doping, the electrical conductivity of PPy is affected by the
    type of oxidant, the initial oxidant/pyrrole molar ratio, synthesis
    procedure, reaction duration, and temperature. The surfactant
    concentration also plays a significant role in the formation of PPy
    structures with different morphologies. The electrical conductivity of
    PPy synthesized using oxidants such as FeCl<sub>3</sub> or ammonium
    persulfate increases with the temperature of the reaction.[20] The
    electrical conductivity of PPy synthesized using FeCl3 as an oxidant was
    higher than when synthesized with ammonium persulfate as an oxidant,
    both in the presence of an anionic surfactant such as sodium dodecyl
    sulfate. This may be due to the difference in chemical structure between
    the two oxidants, and the interaction of pyrrole with sodium dodecyl
    sulfate during the synthesis [21, 22]. <strong>Figure 2</strong> shows
    the electrochemical and chemical synthesis methods of PPy.
    </p>
    <p class="paragraph">
    PPy exhibits good biocompatibility, which makes it suitable for
    biomedical applications. It can be utilized in drug delivery systems,
    tissue engineering scaffolds, and bioelectrodes due to its ability to
    interface with biological systems. The biocompatible nature of PPy,
    combined with its electrical conductivity, provides opportunities for
    bioelectronic devices and neural interfaces [23].
    </p>
    <p class="paragraph">
    As with any material, there are ongoing research efforts to optimize and
    improve the performance of PPy. Scientists are exploring various
    strategies to enhance its conductivity, tailor its properties, and
    expand its functionality. This includes incorporating different dopants,
    composites, and nanostructures into the PPy matrix to achieve specific
    characteristics and performance enhancements.
    </p>
    <p class="paragraph">
    In conclusion, PPy stands as a highly versatile and intriguing
    conducting polymer, renowned for its exceptional electrical
    conductivity, redox properties, processability, and optical
    characteristics. Its broad range of applications spans from electronics
    and optoelectronics to biomedicine, offering promising avenues for
    technological advancements and innovations.
    </p>
    <p class="paragraph"></p>
    <figure>
    <img class="fake"
      src="/images/image006.jpg"
      alt="image006.jpg"
      data-id="8454b6b9-edb9-4e4b-b23f-1b281185f829"
      data-fileid="c53622cb-2119-4184-9b55-65d8786af9f4"
    />
    <figcaption id="8454b6b9-edb9-4e4b-b23f-1b281185f829" class="decoration">
      <strong>Figure 2</strong>. Electrochemical <strong>(A)</strong>, and
      chemical <strong>(B)</strong> synthesis methods of polypyrrole.
      Reprinted with permission from [24]
    </figcaption>
    </figure>
    <h3>Polythiophene (PT)</h3>
    <p class="paragraph">
    Polythiophene (PT) is a type of conjugated polymer that exhibits stable
    conductivity and high electrical conductivity [5]. PT is nontransparent
    and refractory, but its transparency can be increased by dilution
    through various methods such as block copolymerization, alkyl side chain
    grafting, blending with a transparent polymer, producing composites,
    plasma polymerization, electrochemical procedures, and thin layer PT
    deposition [25].
    </p>
    <p class="paragraph">
    PT has garnered considerable interest in both research and industrial
    sectors due to its exceptional environmental stability, improved thermal
    stability, and reduced band gap energy. These desirable properties,
    coupled with their semiconducting, electronic, and optical activities,
    as well as their superior mechanical characteristics and ease of
    processing, have generated significant attention towards PT composites.
    PT has found extensive application in solar cells due to its ability to
    establish improved contact with metal electrodes and its stability under
    ambient conditions. The PT matrix serves as an efficient polymer for
    transporting holes, making it highly compatible with semiconducting
    particles. This combination creates a new hybrid variety with
    exceptional electrical properties. Furthermore, PT stands out as an
    excellent ECPs, due to the conjugated double bonds in the polymer
    backbone. Similar to other conducting polymers, the conductivity of the
    PT matrix can be enhanced by inducing polarons and bipolarons through
    oxidation or reduction processes. This flexibility allows for the
    utilization of PT in various applications such as polymer batteries,
    electrochromic devices, and solar cells. In summary, the unique
    properties of PT, including its compatibility with metal electrodes,
    stability, hole transport capability, optical transparency, and
    intrinsic conductivity, have made it highly sought-after for use in
    solar cells, electrochromic devices, and polymer batteries. By
    leveraging these distinctive characteristics, PT has demonstrated its
    potential for enhancing electrical and optical performances in these
    technologies. Furthermore, the simplicity of polymerization and the air
    stability of PT makes it even more significant compared to other
    conducting polymers [26, 27].
    </p>
    <h3>Poly(p-phenylene) (PPP)</h3>
    <p class="paragraph">
    Poly(p-phenylene) (PPP) is a conducting polymer that has attracted
    considerable attention due to its excellent electrical conductivity. PPP
    is composed of repeating units of p-phenylene, which consists of benzene
    rings connected by carbon-carbon single bonds. PPP exhibits high
    conductivity primarily due to its extended conjugated structure. The
    delocalized π-electron system along the polymer chain allows for the
    efficient movement of electrons, facilitating electrical conductivity.
    The alternating single and double bonds in the p-phenylene units create
    a favorable pathway for electron delocalization and transport [28].
    </p>
    <p class="paragraph">
    To enhance the conductivity of PPP, various materials have been
    hybridized with it, including zinc oxide nanoparticles, silicate
    platelets, polystyrene mixtures, amino or carboxyl groups,
    poly-L-lysine, and (diphenylamino)-s-triazine. Moreover, the electrical
    conductivity of PPP can be further enhanced by controlling its oxidation
    state. By oxidizing the polymer through chemical or electrochemical
    methods, PPP can undergo doping, resulting in the introduction of
    charged species into the polymer structure. This process, known as
    p-doping, increases the number of charge carriers and thus enhances the
    electrical conductivity of PPP. The conductivity of PPP can also be
    influenced by the presence of impurities, defects, or dopants in the
    polymer matrix. The introduction of dopant molecules can lead to the
    formation of charge transfer complexes, which contribute to enhanced
    electrical conductivity. Techniques such as fluorination and sulfonation
    have also been employed [14, 29, 30].
    </p>
    <p class="paragraph">
    The high conductivity of PPP makes it suitable for various electronic
    and optoelectronic applications. It can be utilized as a conductive
    coating or electrode material, as well as in organic electronic devices
    such as transistors, sensors, and light-emitting diodes (LEDs). PPP's
    conductivity, combined with its good processability and mechanical
    properties, makes it a promising candidate for integrating electronic
    functionalities into flexible and lightweight devices. However, it is
    worth noting that the electrical conductivity of PPP can be influenced
    by factors such as molecular weight, crystallinity, processing
    conditions, and the presence of defects. Researchers are actively
    exploring different strategies to further improve the electrical
    conductivity of PPP and optimize its performance in various applications
    [30-32].
    </p>
    <p class="paragraph">
    In summary, PPP is a conducting polymer with remarkable electrical
    conductivity. Its extended conjugated structure and ability to undergo
    doping enable efficient charge transport. The high conductivity of PPP
    opens up opportunities for its utilization in electronic and
    optoelectronic devices, contributing to advancements in the field of
    organic electronics.
    </p>
    <h3>Polyaniline (PANI)</h3>
    <p class="paragraph">
    Polypyrrole (PPy) is a type of polymer that has gained attention for its
    excellent electrical conductivity, which makes it a promising candidate
    for electronic applications. The polymer’s high conductivity is
    primarily attributed to the polymer backbones that facilitate the easy
    movement of electrons within and between them [15]. PPy has a conjugated
    backbone with alternating single and double bonds with
    sp<sup>2</sup>-hybridized carbon atoms. The electrical conductivity of
    PPy changes from an insulator to a semiconductor, with a band gap &lt;
    2.5 eV, once it is doped [16].
    </p>
    <p class="paragraph">
    The doping process creates polarons and bipolaron species that enhance
    the electrical conductivity of PPy. The exact mechanism of charge
    transfer in a PPy chain has not been fully elucidated, but it is
    believed that polarons and bipolarons are created during the doping
    process [17].
    </p>
    <p class="paragraph">
    Polarons are π-conjugated structures based on aromatic rings that are
    formed from the elimination of an electron from the p system of PPy,
    generating a free radical and a cation [18]. The radical cation then
    undergoes a bond rearrangement to form a quinonoid system. In general,
    species with two charges in the same unit cell are termed a bipolaron,
    regardless of whether the species are stable or not. Doping may be
    performed chemically, electrochemically, or by using photo-doping, and
    organic and inorganic dopants have been used for doping PPy [19]. Small
    and large molecule dopants modulate the electrical conductivity and the
    surface structural properties of PPy in different fashions, with larger
    dopants enhancing the electrochemical stability of the synthesized PPy.
    </p>
    <p class="paragraph">
    Apart from doping, the electrical conductivity of PPy is affected by the
    type of oxidant, the initial oxidant/pyrrole molar ratio, synthesis
    procedure, reaction duration, and temperature. The surfactant
    concentration also plays a significant role in the formation of PPy
    structures with different morphologies. The electrical conductivity of
    PPy synthesized using oxidants such as FeCl<sub>3</sub> or ammonium
    persulfate increases with the temperature of the reaction.[20] The
    electrical conductivity of PPy synthesized using FeCl3 as an oxidant was
    higher than when synthesized with ammonium persulfate as an oxidant,
    both in the presence of an anionic surfactant such as sodium dodecyl
    sulfate. This may be due to the difference in chemical structure between
    the two oxidants, and the interaction of pyrrole with sodium dodecyl
    sulfate during the synthesis [21, 22]. <em>Figure 2</em> shows the
    electrochemical and chemical synthesis methods of PPy.
    </p>
    <p class="paragraph">
    PPy exhibits good biocompatibility, which makes it suitable for
    biomedical applications. It can be utilized in drug delivery systems,
    tissue engineering scaffolds, and bioelectrodes due to its ability to
    interface with biological systems. The biocompatible nature of PPy,
    combined with its electrical conductivity, provides opportunities for
    bioelectronic devices and neural interfaces [23].
    </p>
    <p class="paragraph">
    As with any material, there are ongoing research efforts to optimize and
    improve the performance of PPy. Scientists are exploring various
    strategies to enhance its conductivity, tailor its properties, and
    expand its functionality. This includes incorporating different dopants,
    composites, and nanostructures into the PPy matrix to achieve specific
    characteristics and performance enhancements.
    </p>
    <p class="paragraph">
    In conclusion, PPy stands as a highly versatile and intriguing
    conducting polymer, renowned for its exceptional electrical
    conductivity, redox properties, processability, and optical
    characteristics. Its broad range of applications spans from electronics
    and optoelectronics to biomedicine, offering promising avenues for
    technological advancements and innovations.
    </p>
    <figure>
    <img class="fake"
      src="/images/image007.png"
      alt="image007.png"
      data-id=""
      data-fileid="42c4d549-cffb-4a2d-ad0a-fe1341299301"
    />
    <figcaption id="" class="decoration">
      <strong>Figure 2</strong>. Electrochemical <strong>(A)</strong>, and
      chemical <strong>(B)</strong> synthesis methods of polypyrrole.
      Reprinted with permission from [24]
    </figcaption>
    </figure>
    <p class="paragraph"></p>
    <h3>Polythiophene (PT)</h3>
    <p class="paragraph">
    Polythiophene (PT) is a type of conjugated polymer that exhibits stable
    conductivity and high electrical conductivity [5]. PT is nontransparent
    and refractory, but its transparency can be increased by dilution
    through various methods such as block copolymerization, alkyl side chain
    grafting, blending with a transparent polymer, producing composites,
    plasma polymerization, electrochemical procedures, and thin layer PT
    deposition [25].
    </p>
    <p class="paragraph">
    PT has garnered considerable interest in both research and industrial
    sectors due to its exceptional environmental stability, improved thermal
    stability, and reduced band gap energy. These desirable properties,
    coupled with their semiconducting, electronic, and optical activities,
    as well as their superior mechanical characteristics and ease of
    processing, have generated significant attention towards PT composites.
    PT has found extensive application in solar cells due to its ability to
    establish improved contact with metal electrodes and its stability under
    ambient conditions. The PT matrix serves as an efficient polymer for
    transporting holes, making it highly compatible with semiconducting
    particles. This combination creates a new hybrid variety with
    exceptional electrical properties. Furthermore, PT stands out as an
    excellent ECPs, due to the conjugated double bonds in the polymer
    backbone. Similar to other conducting polymers, the conductivity of the
    PT matrix can be enhanced by inducing polarons and bipolarons through
    oxidation or reduction processes. This flexibility allows for the
    utilization of PT in various applications such as polymer batteries,
    electrochromic devices, and solar cells. In summary, the unique
    properties of PT, including its compatibility with metal electrodes,
    stability, hole transport capability, optical transparency, and
    intrinsic conductivity, have made it highly sought-after for use in
    solar cells, electrochromic devices, and polymer batteries. By
    leveraging these distinctive characteristics, PT has demonstrated its
    potential for enhancing electrical and optical performances in these
    technologies. Furthermore, the simplicity of polymerization and the air
    stability of PT makes it even more significant compared to other
    conducting polymers [26, 27].
    </p>
    <h3>Poly(p-phenylene) (PPP)</h3>
    <p class="paragraph">
    Poly(p-phenylene) (PPP) is a conducting polymer that has attracted
    considerable attention due to its excellent electrical conductivity. PPP
    is composed of repeating units of p-phenylene, which consists of benzene
    rings connected by carbon-carbon single bonds. PPP exhibits high
    conductivity primarily due to its extended conjugated structure. The
    delocalized π-electron system along the polymer chain allows for the
    efficient movement of electrons, facilitating electrical conductivity.
    The alternating single and double bonds in the p-phenylene units create
    a favorable pathway for electron delocalization and transport [28].
    </p>
    <p class="paragraph">
    To enhance the conductivity of PPP, various materials have been
    hybridized with it, including zinc oxide nanoparticles, silicate
    platelets, polystyrene mixtures, amino or carboxyl groups,
    poly-L-lysine, and (diphenylamino)-s-triazine. Moreover, the electrical
    conductivity of PPP can be further enhanced by controlling its oxidation
    state. By oxidizing the polymer through chemical or electrochemical
    methods, PPP can undergo doping, resulting in the introduction of
    charged species into the polymer structure. This process, known as
    p-doping, increases the number of charge carriers and thus enhances the
    electrical conductivity of PPP. The conductivity of PPP can also be
    influenced by the presence of impurities, defects, or dopants in the
    polymer matrix. The introduction of dopant molecules can lead to the
    formation of charge transfer complexes, which contribute to enhanced
    electrical conductivity. Techniques such as fluorination and sulfonation
    have also been employed [14, 29, 30].
    </p>
    <p class="paragraph">
    The high conductivity of PPP makes it suitable for various electronic
    and optoelectronic applications. It can be utilized as a conductive
    coating or electrode material, as well as in organic electronic devices
    such as transistors, sensors, and light-emitting diodes (LEDs). PPP’s
    conductivity, combined with its good processability and mechanical
    properties, makes it a promising candidate for integrating electronic
    functionalities into flexible and lightweight devices. However, it is
    worth noting that the electrical conductivity of PPP can be influenced
    by factors such as molecular weight, crystallinity, processing
    conditions, and the presence of defects. Researchers are actively
    exploring different strategies to further improve the electrical
    conductivity of PPP and optimize its performance in various applications
    [30–32].
    </p>
    <p class="paragraph">
    In summary, PPP is a conducting polymer with remarkable electrical
    conductivity. Its extended conjugated structure and ability to undergo
    doping enable efficient charge transport. The high conductivity of PPP
    opens up opportunities for its utilization in electronic and
    optoelectronic devices, contributing to advancements in the field of
    organic electronics.
    </p>
    <h3>Polyacetylene (PA)</h3>
    <p class="paragraph">
    Polyacetylene (PA) is a macromolecule that has garnered much attention
    in the scientific community due to its unique and multifaceted
    properties. It is a conjugated polymer that displays electrical
    conductivity, photoconductivity, gas permeability, supramolecular
    assemblies, chiral recognition, helical graphitic nanofiber formation,
    and liquid crystal behavior. The chemical structure of PA is a linear
    polyene chain, where the backbone provides a platform for decoration
    with pendants. The presence of repeated units of two hydrogen atoms in
    the backbone allows for the substitution of hydrogen with one or two
    substitutes, resulting in monosubstituted or disubstituted PAs,
    respectively (<em>Figure 3).</em>. PA can be synthesized using various
    methods. One of the methods is Ziegler-Natta catalysis, which involves
    the use of titanium and aluminum in the presence of gaseous acetylene
    [33]. By adjusting the temperature and amount of catalyst, PA can be
    synthesized while monitoring the structure and final polymer products.
    Alternative methods of polymerization radiations, such as glow
    discharge, ultraviolet, and Y-radiation, could also be used to
    synthesize PA, which eliminates the use of catalysts and solvents [34].
    </p>
    <figure>
    <img class="fake"
      src="https://kotahi-s3.cloud68.co/kotahi-universcipress/69f7b84c9124_medium.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=zlqRHjmWNyrcCrrAKvOPjuwuBGgmxE%2F20231206%2Fus-east-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20231206T101606Z&amp;X-Amz-Expires=86400&amp;X-Amz-Signature=de445e3b3d207b9d68bbb1fe9c73a0d9dc94669b8cf0f3920f4b9c46d388e90e&amp;X-Amz-SignedHeaders=host"
      alt="Image2.png"
      data-id=""
      data-fileid="6369a2e5-3a0b-4582-8a45-91df2af65821"
    />
    <figcaption id="" class="decoration">
      <em>Figure 3.</em> Mechanism of electrical conductivity of
      polyacetylene. Reprinted with permission from [35]
    </figcaption>
    </figure>
    <h3>Properties and applications of ECPs</h3>
    <p class="paragraph">
    The structure and properties of ECPs play a crucial role in determining
    their performance in biomedical applications. ECPs typically possess
    alternating single and double bonds, which form π-conjugated systems
    that give them unique properties in electrical and electrochemical
    applications. The presence of π-conjugated systems within the polymer
    structure allows for the delocalization of electrons, resulting in
    electrical conductivity. The extent of conjugation and the degree of
    doping heavily influence the conductivity of the polymer. Higher
    conductivity is desirable for applications such as electrodes,
    biosensors, and neural interfaces, where efficient electrical
    communication is required [30, 36–38].
    </p>
    <p class="paragraph">
    The most significant factors that influence ECPs are their degree of
    crystallinity, conjugation length, and interactions within and between
    chains. These aspects have been identified as having the most
    significant impact on the properties of ECPs [39]. Moreover, the
    mechanical properties of ECPs, including flexibility, elasticity, and
    strength, determine their ability to withstand deformations and
    mechanical stresses. Flexible and stretchable polymers are desirable for
    applications in wearable devices, implantable electronics, and tissue
    engineering scaffolds to ensure compatibility with the surrounding
    tissues and enhance long-term performance [40, 41].
    </p>
    <p class="paragraph">
    The electrical conductivity of a substance is determined by its
    structure, which can be categorized into three types: conductive,
    semiconductive, and insulating. The energy band theory is useful in
    classifying materials based on their electrical properties [42].
    Conductive materials have an overlapping valence and conduction band,
    allowing electrons to move freely in the conduction band. In
    semiconductors, the energy gap between the valence and conduction bands
    is small, allowing for intrinsic electron transfer with low energy.
    Insulators have a large energy gap between the two bands, making it
    difficult for electrons to move through the material. However, these
    categories do not necessarily apply to ECPs, which require
    molecular-level studies to determine their conductivity [18, 43]. ECPs
    achieve electrical conductivity through the sharing or propagation of
    charges along their backbone, which consists of alternating single and
    double bonds. This creates an unpaired π electron per carbon atom, and
    the sp2pz hybrid configuration of carbon atoms allows for delocalized
    electrons within the polymer chain. This results in charge mobility
    throughout the polymer structure, enabling features such as electrical
    conductivity, low energy light transmission, and low ionization
    potential [44].
    </p>
    <p class="paragraph">
    The presence of both single and double bonds, including σ and π-bonds,
    allows for electron flow and electrical conductivity in ECPs [30, 45].
    To enhance their electrical conductivity, dopant materials can be
    introduced to these polymers. The addition of various chemical species
    during the polymerization or synthesis process can lead to the formation
    of nonlinear defects such as solitons, poles, dipoles, and electrical
    contributions [46, 47]. These dopant materials can be adjusted to
    control the density and mobility of charge carriers.
    </p>
    <p class="paragraph">
    The electrical conductivity of polymers is dependent on several factors,
    including the degree of doping, the arrangement of the polymer chains,
    the conjugation length of the polymer composition, and the purity of the
    samples. It should also be noted that the methods used to produce high
    conductivity in polymers and inorganic semiconductors differ, and thus
    different conductivities can be achieved depending on the percentage of
    doping species present in the degraded polymers [47].
    </p>
    <p class="paragraph">
    To gain a deeper understanding of the electronic structure of ECPs, it
    is essential to investigate their optical properties. The
    spectrophotometric spectrum can reveal the color and electron spectrum
    of polymers, particularly when associated with other compounds such as
    dopants. The variation of optical spectra during the doping process is
    crucial as it can provide insights into the nature and mechanism of
    charged species in the polymer chain [48].
    </p>
    <p class="paragraph">
    Conjugated polymers have large and rapid nonlinear light response
    properties that can be fine-tuned through adjustable solution
    processing. The degree of electron localization (conjugation) and the
    combination of transition metals with polarizable electrons can increase
    the hyperpolarizability of materials. The nonlinear optical response of
    conjugated polymers depends on several factors, including the conjugate
    length, the orientation of the polymer skeleton, and the relative
    orientation and structural properties of the chromophore complex and
    Schiff base metal complexes. The presence of π-bonds in the skeleton of
    conjugated polymers enables photon-electron interaction and shows an
    anisotropic electron or quasi-one-dimensional structure [49].
    </p>
    <p class="paragraph">
    The electronic behavior of organic semiconductors is influenced by the
    effect of charge stimuli, such as solitons, polarons, or bipolarons in
    the ground state degeneracy. In conjugated polymeric skeletons, the
    optical transitions occur due to the release of charge from dopants or
    from the transfer of oscillator strength from π to π*. Conjugated
    polymers exhibit behavior similar to semiconductors in their pristine
    state and behave like metals when doped with dopants (n-type or p-type)
    [33].
    </p>
    <p class="paragraph">
    ECPs have gained significant attention in the biomedical sector due to
    their unique properties and advantages over traditional materials
    including: (i) Low cost: ECPs can often be synthesized using relatively
    inexpensive methods and materials, which can lead to cost-effective
    production compared to traditional materials like metals or inorganic
    semiconductors. This advantage contributes to the potential for
    widespread adoption of ECPs in the biomedical sector [50–52]; (ii) Low
    detection limit: ECPs can be used to prepare biosensors that enable
    real-time, point-of-care testing. These devices have a low detection
    limit, which can ultimately result in increased access to treatment
    [50]; (iii) Chemical stability: Polymeric materials possess chemical
    stability, which makes them more durable and long-lasting [48]; (iv)
    Good processability: ECPs can be processed using various techniques,
    including solution casting, 3D printing, and electrospinning. This
    processability allows for the fabrication of complex shapes, structures,
    and patterns, making them suitable for applications that require precise
    geometries, such as tissue engineering scaffolds or microfluidic devices
    [48, 53]; (v) Tunable electrical properties: ECPs have tunable
    electrical properties, which means they can be tailored to meet specific
    requirements for different biomedical applications. This property allows
    for the development of electronic components, such as sensors,
    electrodes, and actuators, which are crucial for many biomedical
    applications [30]; (vi) Stimuli Responsiveness: Some ECPs exhibit
    stimuli responsiveness, meaning their electrical or mechanical
    properties can be altered in response to external stimuli, such as
    temperature, pH, or light. This property can be harnessed for
    applications like drug delivery systems, where controlled release of
    medication can be achieved by applying a specific stimulus [40, 52].
    </p>
    <p class="paragraph">
    The development of ECPs for biomedical applications is accompanied by
    several challenges. Ensuring biocompatibility is a significant challenge
    for ECPs. Some polymers may exhibit inherent cytotoxicity or cause
    adverse reactions when in contact with living tissues. Modifying ECPs
    can be done in several ways to enhance their biocompatibility and reduce
    toxicity: (i) Surface Modification: Modifying the surface of ECPs is a
    common approach to improving their biocompatibility. This can be
    achieved by functionalizing the polymer surface with bioactive
    molecules, such as peptides, proteins, or carbohydrates. These bioactive
    moieties can promote cell adhesion, prevent protein adsorption, and
    modulate the immune response, leading to enhanced biocompatibility [40,
    41, 54]; (ii) Polymer Blending: Blending ECPs with biocompatible
    polymers can result in hybrid materials with improved biocompatibility.
    By combining the advantageous properties of both polymers, such as
    electrical conductivity and biocompatibility, the resulting blend can
    exhibit enhanced performance in biomedical applications. The choice of
    the biocompatible polymer and the blending ratio should be carefully
    optimized to achieve the desired properties [54–56]; (iii)
    Preconditioning and Surface Treatments: Preconditioning techniques such
    as sterilization, plasma treatment, or UV irradiation can modify the
    surface properties of ECPs, improving their biocompatibility. These
    treatments can alter surface chemistry, charge, or roughness, thereby
    reducing toxicity and promoting favorable cellular responses [57–60].
    </p>
    <p class="paragraph">
    Moreover, ECPs may be susceptible to degradation, especially under
    physiological conditions or in the presence of reactive species. This
    can lead to a loss of electrical conductivity or structural integrity
    over time. To address this challenge, researchers can explore strategies
    such as polymer synthesis optimization, crosslinking, encapsulation, or
    the use of protective coatings to enhance stability and resistance to
    degradation [40, 41, 54, 59].
    </p>
    <p class="paragraph">
    While ECPs may possess good electrical conductivity, their mechanical
    properties, such as strength, flexibility, or stretchability, may need
    improvement to meet the demands of biomedical applications. Strategies
    like polymer blending with elastomers or reinforcement with nanofillers
    can enhance the mechanical properties and tailor them to specific
    application requirements [40, 41, 51, 55, 61].
    </p>
    <p class="paragraph">
    The long-term performance and stability of ECPs in the body are also
    essential for successful biomedical applications. Factors such as
    biostability, resistance to fatigue, and biodegradation (if desired)
    need to be carefully considered and addressed during the design phase.
    Long-term in vivo studies can provide insights into the behavior of ECPs
    over extended periods and help identify potential challenges and
    necessary improvements [59, 62, 63].
    </p>
    <h2>Future perspective of ECPs in the biomedical sector</h2>
    <p class="paragraph">
    The future of ECPs in the biomedical sector holds great promise. As
    research and development efforts continue, we can expect significant
    advancements and the emergence of new applications. Here are some
    potential areas where ECPs could have a notable impact in the near
    future: (i) Implantable Medical Devices: ECPs can revolutionize the
    field of implantable medical devices, such as neural interfaces,
    bioelectrodes, and cardiac devices. These polymers can provide enhanced
    biocompatibility, flexibility, and conductivity, enabling improved
    long-term performance and biointegration. They can facilitate better
    communication with the body’s tissues and nerves, allowing for more
    precise control and feedback in therapeutic interventions [64–66]; (ii)
    Tissue Engineering and Regenerative Medicine: ECPs have the potential to
    play a critical role in tissue engineering and regenerative medicine. By
    incorporating electroconductive scaffolds or hydrogels, these polymers
    can provide electrical cues to guide cell behavior and enhance tissue
    regeneration. They can aid in the development of functional engineered
    tissues, such as cardiac patches, neural grafts, or muscle constructs,
    with the ability to integrate and communicate with the surrounding host
    tissue [40, 54, 59, 67]; (iii) Wearable and Flexible Electronics: The
    field of wearable electronics can benefit significantly from the
    properties of ECPs. These polymers can enable the development of
    flexible, stretchable, and conformable electronic devices that can be
    comfortably worn on the body. Applications could include smart textiles,
    wearable biosensors for health monitoring, electronic skin for
    prosthetics, and advanced human-machine interfaces [68–71]; (iv)
    Controlled Drug Delivery Systems: ECPs can be utilized in the
    development of controlled drug delivery systems. By incorporating drugs
    or therapeutic agents within the polymer matrix, they can enable
    controlled and localized release based on external stimuli or specific
    physiological conditions. These systems can improve the efficacy and
    safety of drug delivery, particularly in areas such as cancer therapy,
    chronic pain management, or wound healing [72–75]; (v) Bioelectronics
    and Bioresponsive Interfaces: ECPs can be employed in the development of
    bioelectronic interfaces that bridge the gap between biological systems
    and electronic devices. They can enable direct communication and signal
    transduction with biological entities, such as neurons or cells, for
    applications in neuroprosthetics, bioelectronic implants, or
    brain-computer interfaces. The unique properties of ECPs can facilitate
    the development of bioresponsive interfaces that can sense and adapt to
    dynamic physiological conditions [54, 55, 59, 61]; (vi) Biosensors and
    Diagnostics: ECPs can be utilized in the development of biosensors and
    diagnostic devices for various healthcare applications. These polymers
    can serve as transducing elements in sensors, enabling the detection and
    quantification of biomarkers or analytes. Their electrical conductivity
    can be modulated by the presence of specific molecules, facilitating
    highly sensitive and selective detection, potentially leading to
    advancements in point-of-care diagnostics and personalized medicine [40,
    55, 61, 76].
    </p>
    <p class="paragraph">
    It is important to note that while these potential applications hold
    promise, further research, development, and validation are necessary to
    ensure the efficacy, safety, and reliability of ECPs in real-world
    biomedical settings. Nonetheless, the unique properties of ECPs make
    them a highly promising class of materials with the potential to
    revolutionize various aspects of healthcare and improve patient
    outcomes.
    </p>
    <h2>Conclusions</h2>
    <p class="paragraph">
    In conclusion, ECPs are emerging as a fascinating class of materials
    with a unique set of properties, extending their appeal to a broad
    spectrum of applications. This review serves as a comprehensive resource
    encompassing the synthesis, characteristics, and implementation of some
    of the most extensively studied ECPs, namely PANI, PPy, PT, PPP, and PA.
    The discourse underlines the significance of comprehending the intrinsic
    link between the structure and properties of ECPs, whilst considering
    how varying factors such as dopants, polymerization techniques, and
    processing conditions can alter their attributes. Furthermore, the
    review furnishes an exhaustive insight into the application of ECPs in
    diverse biomedical sectors, from biosensors to tissue engineering,
    highlighting recent strides and existing challenges in these fields. As
    the realm of ECP research continues to expand, upcoming endeavors should
    concentrate on devising innovative materials and fabrication
    methodologies for sophisticated applications. Given their potential to
    dramatically reshape sectors including electronics, energy, and
    healthcare, ECPs constitute a compelling and pivotal field within
    materials science. Anticipation surrounds the future of ECPs, as they
    are poised to continue drawing considerable interest in the foreseeable
    future.
    </p>
    <h2>Authors’ contributions</h2>
    <p class="paragraph">
    All authors contributed to drafting, and revising of the paper and
    agreed to be responsible for all the aspects of this work.
    </p>
    <h2>Declaration of competing interest</h2>
    <p class="paragraph">The authors declare no competing interest.</p>
    <h2>Funding</h2>
    <p class="paragraph">This paper received no external funding.</p>
    <h2>Data availability</h2>
    <p class="paragraph">Not applicable.</p>
    <h2>References</h2>
    <p class="paragraph">
    [1] D. Ghosh, D. Khastgir, Degradation and Stability of Polymeric
    High-Voltage Insulators and Prediction of Their Service Life through
    Environmental and Accelerated Aging Processes, ACS Omega 3 (2018)
    11317–11330.
    </p>
    <p class="paragraph">
    [2] M.&nbsp;Z. Saleem, M. Akbar, Review of the Performance of
    High-Voltage Composite Insulators, Polymers 14 (2022) 431.
    </p>
    <p class="paragraph">
    [3] T. Nezakati, A. Seifalian, A. Tan, A.M. Seifalian, Conductive
    Polymers: Opportunities and Challenges in Biomedical Applications, Chem.
    Rev. 118 (2018) 6766–6843.
    </p>
    <p class="paragraph">
    [4] E.&nbsp;N. Zare, P. Makvandi, B. Ashtari, F. Rossi, A. Motahari, G.
    Perale, Progress in Conductive Polyaniline-Based Nanocomposites for
    Biomedical Applications: A Review, J. Med. Chem. 63 (2020) 1–22.
    </p>
    <p class="paragraph">
    [5] M. Shanmugam, A. Augustin, S. Mohan, B. Honnappa, C. Chuaicham, S.
    Rajendran, T.&nbsp;K.&nbsp;A. Hoang, K. Sasaki, K. Sekar, Conducting
    polymeric nanocomposites: A review in solar fuel applications, Fuel 325
    (2022) 124899.
    </p>
    <p class="paragraph">
    [6] B. Guo, P.&nbsp;X. Ma, Conducting Polymers for Tissue Engineering,
    Biomacromolecules 19 (2018) 1764–1782.
    </p>
    <p class="paragraph">
    [7] M. Baghayeri, E.&nbsp;N. Zare, M.&nbsp;M. Lakouraj, Monitoring of
    hydrogen peroxide using a glassy carbon electrode modified with
    hemoglobin and a polypyrrole-based nanocomposite, Microchim. Acta 182
    (2015) 771–779.
    </p>
    <p class="paragraph">
    [8] M. Baghayeri, E. Nazarzadeh Zare, M. Mansour Lakouraj, A simple
    hydrogen peroxide biosensor based on a novel electro-magnetic
    poly(p-phenylenediamine)@Fe3O4 nanocomposite, Biosens. Bioelectron. 55
    (2014) 259–265.
    </p>
    <p class="paragraph">
    [9] M. Ghovvati, M. Kharaziha, R. Ardehali, N. Annabi, Recent Advances
    in Designing Electroconductive Biomaterials for Cardiac Tissue
    Engineering, Adv. Healthc. Mater. 11 (2022) 2200055.
    </p>
    <p class="paragraph">
    [10] S. Sarkar, N. Levi-Polyachenko, Conjugated polymer nano-systems for
    hyperthermia, imaging and drug delivery, Adv. Drug Deliv. Rev. 163–164
    (2020) 40–64.
    </p>
    <p class="paragraph">
    [11] A.&nbsp;N. Gheymasi, Y. Rajabi, E.&nbsp;N. Zare, Nonlinear optical
    properties of poly(aniline-co-pyrrole)@ZnO-based nanofluid, Opt. Mater.
    102 (2020) 109835.
    </p>
    <p class="paragraph">
    [12] E.&nbsp;N. Zare, M.&nbsp;M. Lakouraj, Biodegradable
    polyaniline/dextrin conductive nanocomposites: synthesis,
    characterization, and study of antioxidant activity and sorption of
    heavy metal ions, Iran. Polym. J. 23 (2014) 257–266.
    </p>
    <p class="paragraph">
    [13] P. Najafi Moghadam, E. Nazarzadeh Zareh, Synthesis of conductive
    nanocomposites based on polyaniline/poly(styrene-alt-maleic
    anhydride)/polystyrene, e-Polymers 10 (2010).
    </p>
    <p class="paragraph">
    [14] M. Ghomi, E.&nbsp;N. Zare, R.&nbsp;S. Varma, Preparation of
    Conducting Polymers/Composites, Conductive Polymers in Analytical
    Chemistry, ACS (2022) 67–90.
    </p>
    <p class="paragraph">
    [15] M. Gizdavic-Nikolaidis, J. Travas-Sejdic, G.&nbsp;A. Bowmaker,
    R.&nbsp;P. Cooney, C. Thompson, P.&nbsp;A. Kilmartin, The antioxidant
    activity of conducting polymers in biomedical applications, Curr. Appl.
    Phys. 4 (2004) 347–350.
    </p>
    <p class="paragraph">
    [16] P. Jayamurugan, V. Ponnuswamy, S. Ashokan, Y.&nbsp;V.&nbsp;S. Rao,
    T. Mahalingam, PPy Doped with DBSA and Combined with PSS to Improve
    Processability and Control the Morphology, Int. Polym. Process. 30
    (2015) 422–427.
    </p>
    <p class="paragraph">
    [17] H. Yu, Y. He, H. Li, Z. Li, B. Ren, G. Chen, X. Hu, T. Tang, Y.
    Cheng, J.&nbsp;Z. Ou, Core-shell PPy@TiO2 enable GO membranes with
    controllable and stable dye desalination properties, Desalination 526
    (2022) 115523.
    </p>
    <p class="paragraph">
    [18] J. Nightingale, J. Wade, D. Moia, J. Nelson, J.-S. Kim, Impact of
    Molecular Order on Polaron Formation in Conjugated Polymers, J. Phys.
    Chem. C 122 (2018) 29129–29140.
    </p>
    <p class="paragraph">
    [19] M. Gosh, A. Barman, A.&nbsp;K. Meikap, S.&nbsp;K. De, S.
    Chatterjee, Hopping transport in HCl doped conducting polyaniline, Phys.
    Lett. A 260 (1999) 138–148.
    </p>
    <p class="paragraph">
    [20] A. Yussuf, M. Al-Saleh, S. Al-Enezi, G. Abraham, Synthesis and
    Characterization of Conductive Polypyrrole: The Influence of the
    Oxidants and Monomer on the Electrical, Thermal, and Morphological
    Properties, Int. J. Polym. Sci. 2018 (2018) 4191747.
    </p>
    <p class="paragraph">
    [21] H. Masuda, D.&nbsp;K. Asano, Preparation and properties of
    polypyrrole, Synth. Met. 135–136 (2003) 43–44.
    </p>
    <p class="paragraph">
    [22] E. Nazarzadeh Zare, M. Mansour Lakouraj, M. Mohseni, Biodegradable
    polypyrrole/dextrin conductive nanocomposite: Synthesis,
    characterization, antioxidant and antibacterial activity, Synth. Met.
    187 (2014) 9–16.
    </p>
    <p class="paragraph">
    [23] A. Fahlgren, C. Bratengeier, A. Gelmi, C.&nbsp;M. Semeins, J.
    Klein-Nulend, E.&nbsp;W. Jager, A.D. Bakker, Biocompatibility of
    Polypyrrole with Human Primary Osteoblasts and the Effect of Dopants,
    PLoS One 10 (2015) e0134023.
    </p>
    <p class="paragraph">
    [24] E.&nbsp;N. Zare, T. Agarwal, A. Zarepour, F. Pinelli, A. Zarrabi,
    F. Rossi, M. Ashrafizadeh, A. Maleki, M.-A. Shahbazi, T.&nbsp;K. Maiti,
    R.&nbsp;S. Varma, F.&nbsp;R. Tay, M.&nbsp;R. Hamblin, V. Mattoli, P.
    Makvandi, Electroconductive multi-functional polypyrrole composites for
    biomedical applications, Appl. Mater. Today 24 (2021) 101117.
    </p>
    <p class="paragraph">
    [25] A.&nbsp;L. Gomes, M.&nbsp;B. Pinto Zakia, J.&nbsp;G. Filho, E.
    Armelin, C. Alemán, J. Sinezio de Carvalho Campos, Preparation and
    characterization of semiconducting polymeric blends. Photochemical
    synthesis of poly(3-alkylthiophenes) using host microporous matrices of
    poly(vinylidene fluoride), Polym. Chem. 3 (2012) 1334–1343.
    </p>
    <p class="paragraph">
    [26] M.&nbsp;T. Ramesan, K. Suhailath, 13 - Role of nanoparticles on
    polymer composites, in: R.&nbsp;K. Mishra, S. Thomas, N. Kalarikkal
    (Eds.), Micro and Nano Fibrillar Composites (MFCs and NFCs) from Polymer
    Blends, Woodhead Publishing (2017) 301–326.
    </p>
    <p class="paragraph">
    [27] R.&nbsp;P. Pant, S.&nbsp;K. Dhawan, D. Suri, M. Arora, S.&nbsp;K.
    Gupta, M. Koneracká, P. Kopčanský, M. Timko, Synthesis and
    characterization of ferrofluid-conducting polymer composite, Indian J.
    Eng. Mater. Sci. 11 (2004) 267–270.
    </p>
    <p class="paragraph">
    [28] J. Heinze, Electrochemistry of conducting polymers, Synth. Met. 43
    (1991) 2805–2823.
    </p>
    <p class="paragraph">
    [29] S. Tokito, T. Tsutsui, S. Saito, Electrical Conductivity and
    Optical Properties of Poly(p-phenylene sulflde) Doped with Some Organic
    Acceptors, Polym. J. 17 (1985) 959–968.
    </p>
    <p class="paragraph">
    [30] N. K, C.&nbsp;S. Rout, Conducting polymers: a comprehensive review
    on recent advances in synthesis, properties and applications, RSC Adv.
    11 (2021) 5659–5697.
    </p>
    <p class="paragraph">
    [31] H.&nbsp;W. Hill Jr., D.&nbsp;G. Brady, Properties, environmental
    stability, and molding characteristics of polyphenylene sulfide, Polym.
    Eng. Sci. 16 (1976) 831–835.
    </p>
    <p class="paragraph">
    [32] A.&nbsp;S. Rahate, K.&nbsp;R. Nemade, S.&nbsp;A. Waghuley,
    Polyphenylene sulfide (PPS): state of the art and applications, Rev.
    Chem. Eng. 29 (2013) 471–489.
    </p>
    <p class="paragraph">
    [33] D. Yaron, Nonlinear optical response of conjugated polymers:
    Essential excitations and scattering, Phys. Rev. B 54 (1996) 4609–4620.
    </p>
    <p class="paragraph">
    [34] S.&nbsp;H. Cho, K.&nbsp;T. Song, J.&nbsp;Y. Lee, Recent advances in
    polypyrrole (2007) 1–8.
    </p>
    <p class="paragraph">
    [35] A.&nbsp;A. Baleg, M. Masikini, S.&nbsp;V. John, A.&nbsp;R.
    Williams, N. Jahed, P. Baker, E. Iwuoha, Conducting Polymers and
    Composites, in: M.&nbsp;A. Jafar Mazumder, H. Sheardown, A. Al-Ahmed
    (Eds.), Functional Polymers, Springer, Cham. (2018) 1–54.
    </p>
    <p class="paragraph">
    [36] T.-H. Le, Y. Kim, H. Yoon, Electrical and Electrochemical
    Properties of Conducting Polymers, Polymers 9 (2017) 150.
    </p>
    <p class="paragraph">
    [37] S.&nbsp;B. Mdluli, M.&nbsp;E. Ramoroka, S.&nbsp;T. Yussuf,
    K.&nbsp;D. Modibane, V.&nbsp;S. John-Denk, E.&nbsp;I. Iwuoha,
    &amp;pi;-Conjugated Polymers and Their Application in Organic and Hybrid
    Organic-Silicon Solar Cells, Polymers 14 (2022) 716.
    </p>
    <p class="paragraph">
    [38] F. Borrmann, T. Tsuda, O. Guskova, N. Kiriy, C. Hoffmann, D.
    Neusser, S. Ludwigs, U. Lappan, F. Simon, M. Geisler, B. Debnath, Y.
    Krupskaya, M. Al-Hussein, A. Kiriy, Charge-Compensated N-Doped
    π-Conjugated Polymers: Toward both Thermodynamic Stability of N-Doped
    States in Water and High Electron Conductivity, Adv. Sci. 9 (2022)
    2203530.
    </p>
    <p class="paragraph">
    [39] J. Stejskal, M. Trchová, Conducting polypyrrole nanotubes: a
    review, Chem. Pap. 72 (2018) 1563–1595.
    </p>
    <p class="paragraph">
    [40] G. Kaur, R. Adhikari, P. Cass, M. Bown, P. Gunatillake,
    Electrically conductive polymers and composites for biomedical
    applications, RSC Adv. 5 (2015) 37553–37567.
    </p>
    <p class="paragraph">
    [41] H. He, L. Zhang, X. Guan, H. Cheng, X. Liu, S. Yu, J. Wei, J.
    Ouyang, Biocompatible Conductive Polymers with High Conductivity and
    High Stretchability, ACS. Appl. Mater. Interfaces 11 (2019) 26185–26193.
    </p>
    <p class="paragraph">
    [42] T. Pal, S. Banerjee, P.&nbsp;K. Manna, K.&nbsp;K. Kar,
    Characteristics of Conducting Polymers, in: K.&nbsp;K. Kar (Ed.),
    Handbook of Nanocomposite Supercapacitor Materials I: Characteristics,
    Springer, Cham. (2020) 247–268.
    </p>
    <p class="paragraph">
    [43] G. Magela e Silva, Electric-field effects on the competition
    between polarons and bipolarons in conjugated polymers, Phys. Rev. B 61
    (2000) 10777–10781.
    </p>
    <p class="paragraph">
    [44] A.M.&nbsp;R. Ramírez, M.&nbsp;A. Gacitúa, E. Ortega, F.&nbsp;R.
    Díaz, M.&nbsp;A. del Valle, Electrochemical in situ synthesis of
    polypyrrole nanowires, Electrochem. Commun. 102 (2019) 94–98.
    </p>
    <p class="paragraph">
    [45] R. Ravichandran, S. Sundarrajan, J.&nbsp;R. Venugopal, S.
    Mukherjee, S. Ramakrishna, Applications of conducting polymers and their
    issues in biomedical engineering, J.&nbsp;R. Soc. Interface 7(suppl_5)
    (2010) 559–579.
    </p>
    <p class="paragraph">
    [46] B.&nbsp;D. Malhotra, A. Chaubey, S.&nbsp;P. Singh, Prospects of
    conducting polymers in biosensors, Anal. Chim. Acta 578 (2006) 59–74.
    </p>
    <p class="paragraph">
    [47] K.-H. Yang, Y.-C. Liu, C.-C. Yu, Temperature effect of
    electrochemically roughened gold substrates on polymerization
    electrocatalysis of polypyrrole, Anal. Chim. Acta 631 (2009) 40–46.
    </p>
    <p class="paragraph">
    [48] Y. Park, J. Jung, M. Chang, Research Progress on Conducting
    Polymer-Based Biomedical Applications, App. Sci. 9 (2019) 1070.
    </p>
    <p class="paragraph">
    [49] A. Pietrangelo, B.C. Sih, B.&nbsp;N. Boden, Z. Wang, Q. Li,
    K.&nbsp;C. Chou, M.&nbsp;J. MacLachlan, M.&nbsp;O. Wolf, Nonlinear
    Optical Properties of Schiff-Base-Containing Conductive Polymer Films
    Electro-deposited in Microgravity, Adv. Mater. 20 (2008) 2280–2284.
    </p>
    <p class="paragraph">
    [50] D. Runsewe, T. Betancourt, J.&nbsp;A. Irvin, Biomedical Application
    of Electroactive Polymers in Electrochemical Sensors: A Review,
    Materials 12 (2019) 2629.
    </p>
    <p class="paragraph">
    [51] Y. Yan, Y. Jiang, E.&nbsp;L.&nbsp;L. Ng, Y. Zhang, C. Owh, F. Wang,
    Q. Song, T. Feng, B. Zhang, P. Li, X.&nbsp;J. Loh, S.&nbsp;Y. Chan,
    B.&nbsp;Q.&nbsp;Y. Chan, Progress and opportunities in additive
    manufacturing of electrically conductive polymer composites, Mater.
    Today Adv. 17 (2023) 100333.
    </p>
    <p class="paragraph">
    [52] Y. Arteshi, A. Aghanejad, S. Davaran, Y. Omidi, Biocompatible and
    electroconductive polyaniline-based biomaterials for electrical
    stimulation, Eur. Polym. J. 108 (2018) 150–170.
    </p>
    <p class="paragraph">
    [53] E. Łyszczarz, W. Brniak, J. Szafraniec-Szczęsny, T.&nbsp;M. Majka,
    D. Majda, M. Zych, K. Pielichowski, R. Jachowicz, The Impact of the
    Preparation Method on the Properties of Orodispersible Films with
    Aripiprazole: Electrospinning vs. Casting and 3D Printing Methods,
    Pharmaceutics 13 (2021) 1122.
    </p>
    <p class="paragraph">
    [54] M. Bhandari, D.&nbsp;P. Kaur, S. Raj, T. Yadav, M.&nbsp;A.&nbsp;S.
    Abourehab, M.&nbsp;S. Alam, Electrically Conducting Smart Biodegradable
    Polymers and Their Applications, in: G.&nbsp;A.M. Ali,
    A.&nbsp;S.&nbsp;H. Makhlouf (Eds.), Handbook of Biodegradable Materials,
    Springer, Cham. (2023) 391–413.
    </p>
    <p class="paragraph">
    [55] S. Lee, B. Ozlu, T. Eom, D.C. Martin, B.&nbsp;S. Shim, Electrically
    conducting polymers for bio-interfacing electronics: From neural and
    cardiac interfaces to bone and artificial tissue biomaterials, Biosens.
    Bioelectron. 170 (2020) 112620.
    </p>
    <p class="paragraph">
    [56] A. Markov, R. Wördenweber, L. Ichkitidze, A. Gerasimenko, U.
    Kurilova, I. Suetina, M. Mezentseva, A. Offenhäusser, D. Telyshev,
    Biocompatible SWCNT Conductive Composites for Biomedical Applications,
    Nanomaterials 10 (2020) 2492.
    </p>
    <p class="paragraph">
    [57] S.&nbsp;C. Wang, K.&nbsp;S. Chang, C.&nbsp;J. Yuan, Enhancement of
    electrochemical properties of screen-printed carbon electrodes by oxygen
    plasma treatment, Electrochim. Acta 54 (2009) 4937–4943.
    </p>
    <p class="paragraph">
    [58] R. Ghobeira, C. Philips, H. Declercq, P. Cools, N. De Geyter, R.
    Cornelissen, R. Morent, Effects of different sterilization methods on
    the physicochemical and bioresponsive properties of plasma-treated
    polycaprolactone films, Biomed.Mater. 12 (2017) 015017.
    </p>
    <p class="paragraph">
    [59] A.&nbsp;C. da Silva, S.&nbsp;I. Córdoba de Torresi, Advances in
    Conducting, Biodegradable and Biocompatible Copolymers for Biomedical
    Applications, Front. Mater. 6 (2019).
    </p>
    <p class="paragraph">
    [60] S. Cheruthazhekatt, M. Černák, P. Slavíček, J. Havel, Gas plasmas
    and plasma modified materials in medicine, J. Appl. Biomed. 8 (2010)
    55–66.
    </p>
    <p class="paragraph">
    [61] M.&nbsp;N. Barshutina, V.&nbsp;S. Volkov, A.&nbsp;V. Arsenin,
    D.&nbsp;I. Yakubovsky, A.&nbsp;V. Melezhik, A.&nbsp;N. Blokhin,
    A.&nbsp;G. Tkachev, A.&nbsp;V. Lopachev, V.&nbsp;A. Kondrashov,
    Biocompatible, Electroconductive, and Highly Stretchable Hybrid Silicone
    Composites Based on Few-Layer Graphene and CNTs, Nanomaterials (Basel)
    11 (2021).
    </p>
    <p class="paragraph">
    [62] S. Lyu, D. Untereker, Degradability of Polymers for Implantable
    Biomedical Devices, Int. J. Mol. Sci. 10 (2009) 4033–4065.
    </p>
    <p class="paragraph">
    [63] N.&nbsp;J. Lores, X. Hung, M.&nbsp;H. Talou, G.&nbsp;A. Abraham,
    P.&nbsp;C. Caracciolo, Novel three-dimensional printing of poly(ester
    urethane) scaffolds for biomedical applications, Polym. Adv.Technol. 32
    (2021) 3309–3321.
    </p>
    <p class="paragraph">
    [64] M. Pajic, J. Zhihao, A. Connolly, R. Mangharam, A Framework for
    Validation of Implantable Medical Devices, Real-Time and Embedded
    Systems Lab (mLAB) (2010).
    </p>
    <p class="paragraph">
    [65] A.&nbsp;B. Amar, A.&nbsp;B. Kouki, H. Cao, Power Approaches for
    Implantable Medical Devices, Sensors 15 (2015) 28889–28914.
    </p>
    <p class="paragraph">
    [66] M.&nbsp;M.&nbsp;H. Shuvo, T. Titirsha, N. Amin, S.&nbsp;K. Islam,
    Energy Harvesting in Implantable and Wearable Medical Devices for
    Enduring Precision Healthcare, Energies 15 (2022) 7495.
    </p>
    <p class="paragraph">
    [67] H. Esmaeili, A. Patino-Guerrero, M. Hasany, M.&nbsp;O. Ansari, A.
    Memic, A. Dolatshahi-Pirouz, M. Nikkhah, Electroconductive biomaterials
    for cardiac tissue engineering, Acta Biomater. 139 (2022) 118–140.
    </p>
    <p class="paragraph">
    [68] B. Podsiadły, P. Walter, M. Kamiński, A. Skalski, M. Słoma,
    Electrically Conductive Nanocomposite Fibers for Flexible and Structural
    Electronics, Appl. Sci. 12 (2022) 941.
    </p>
    <p class="paragraph">
    [69] S.&nbsp;M.&nbsp;A. Mokhtar, E. Alvarez de Eulate, M. Yamada,
    T.&nbsp;W. Prow, D.&nbsp;R. Evans, Conducting polymers in wearable
    devices, Med. Devices Sens. 4 (2021) e10160.
    </p>
    <p class="paragraph">
    [70] S. Peng, Y. Yu, S. Wu, C.-H. Wang, Conductive Polymer
    Nanocomposites for Stretchable Electronics: Material Selection, Design,
    and Applications, ACS Appl. Mater. Interfaces 13 (2021) 43831–43854.
    </p>
    <p class="paragraph">
    [71] C. Park, M.&nbsp;S. Kim, H.&nbsp;H. Kim, S.-H. Sunwoo, D.&nbsp;J.
    Jung, M.&nbsp;K. Choi, D.-H. Kim, Stretchable conductive nanocomposites
    and their applications in wearable devices, Appl. Phys. Rev. 9 (2022).
    </p>
    <p class="paragraph">
    [72] C.&nbsp;A.&nbsp;R. Chapman, E.&nbsp;A. Cuttaz, J.&nbsp;A. Goding,
    R.&nbsp;A. Green, Actively controlled local drug delivery using
    conductive polymer-based devices, Appl. Phys. Lett. 116 (2020).
    </p>
    <p class="paragraph">
    [73] S. Adepu, S. Ramakrishna, Controlled Drug Delivery Systems: Current
    Status and Future Directions, Molecules 26 (2021) 5905.
    </p>
    <p class="paragraph">
    [74] M.&nbsp;D. Ashton, P.&nbsp;A. Cooper, S. Municoy, M.&nbsp;F.
    Desimone, D. Cheneler, S.&nbsp;D. Shnyder, J.&nbsp;G. Hardy, Controlled
    Bioactive Delivery Using Degradable Electroactive Polymers,
    Biomacromolecules 23 (2022) 3031–3040.
    </p>
    <p class="paragraph">
    [75] M.-L. Laracuente, M.&nbsp;H. Yu, K.&nbsp;J. McHugh, Zero-order drug
    delivery: State of the art and future prospects, J. Control. Release 327
    (2020) 834–856.
    </p>
    <p class="paragraph">
    [76] S. Gungordu Er, A. Kelly, S.&nbsp;B.&nbsp;W. Jayasuriya, M.
    Edirisinghe, Nanofiber Based on Electrically Conductive Materials for
    Biosensor Applications, Biomed. Mater. Devices (2022)
    <a href="https://doi.org/10.1007/s44174-022-00050-z" rel="" target="blank">https://doi.org/10.1007/s44174–022–00050-z</a>.
    </p>
---

    <h2>Introduction</h2>
    <p class="paragraph">
    The majority of polymers are insulators due to the strong covalent
    bonding between atoms in their chains, which results in the absence of
    free-moving electrons or ions [1, 2]. However, for medical implants in
    cardiac, nerve, and skeletal muscle tissues, this lack of electrical
    conductivity presents a significant drawback [3]. To achieve a fully
    functional restoration, medical implants in these tissues need to
    exhibit a certain level of electrical conductivity.
    </p>
    <p class="paragraph">
    Electroconductive polymers (ECPs) are organic materials that possess
    unique electrical and optical properties, similar to those of inorganic
    semiconductors and metals [3, 4]. They can be synthesized using
    cost-effective approaches and can be assembled into supramolecular
    structures with multifunctional capabilities. ECPs have demonstrated
    promising capabilities to induce various cellular mechanisms, which
    broadens their unique applications in the biomedical field [4]. They are
    attractive for various biomedical applications due to their intelligent
    response to electrical fields from different types of tissues. ECPs have
    been used to enhance the electrical sensitivity, speed, and stability of
    various biomedical devices and their interfaces with biological tissues.
    </p>
    <p class="paragraph">
    There are five common types of ECPs: polyaniline, polypyrrole,
    polythiophene, polyphenylene, and polyacetylene. The electrical
    conductivity (S/cm) of these ECPs and their abbreviations are shown in
    <strong>Table 1</strong>. In addition to these polymers, conductive
    composite polymers, such as π-conjugated ECPs, conductive nanoparticles,
    carbon-based nanoparticles, and alloys, have also been extensively
    studied [5].
    </p>
    <p class="paragraph">
    ECPs have several potential applications, including electrically induced
    targeted drug release systems, biosensors, and metabolic markers of
    stress [6]. Moreover, the clinical applications of ECPs are vast,
    including the development of nerve conduits, synthetic scaffolds that
    support the successful regeneration of myocardial tissue, and deep brain
    stimulation for the treatment of neurological disorders such as
    Parkinson's disease. There are still many potential uses of ECPs, from
    biosensors and bioactuators to drug delivery systems and neural
    prosthetics [7-9].
    </p>
    <p class="paragraph">
    This manuscript presents an in-depth study of the structure and
    properties of ECPs, including their electrical and optical properties,
    which directly influence their performance in biomedical applications.
    The manuscript not only discusses the potential applications of ECPs,
    such as in tissue engineering, regenerative medicine, biosensors, and
    neural prosthetics, but also the challenges in their development,
    including the need for biocompatibility, stability, and enhanced
    mechanical properties. Furthermore, the review provides novel insights
    into the methods for modifying ECPs to enhance their biocompatibility
    and reduce toxicity. It discusses innovative approaches such as surface
    modification, polymer blending, and preconditioning techniques. The
    manuscript also brings to light the key advantages of ECPs in the
    biomedical field, such as cost-effectiveness, low detection limit for
    biosensors, chemical stability, good processability, tunable electrical
    properties, and stimuli responsiveness. In essence, this manuscript
    presents a novel, well-rounded perspective on the use and potential of
    ECPs in the biomedical field, making it a valuable resource for
    researchers and industry professionals alike.
    </p>
    <h3>
    <strong>Table 1</strong>. Conductivities of common types of
    electroconductive polymers [3]
    </h3>
    <table>
    <tbody>
      <tr>
        <td>
          <p class="paragraph">
            <strong>Electroconductive polymers</strong>
          </p>
        </td>
        <td>
          <p class="paragraph"><strong>Abbreviation</strong></p>
        </td>
        <td>
          <p class="paragraph"><strong>Formula</strong></p>
        </td>
        <td>
          <p class="paragraph"><strong>Conductivity (S/cm)</strong></p>
        </td>
      </tr>
      <tr>
        <td><p class="paragraph">Polyaniline</p></td>
        <td><p class="paragraph">PANI</p></td>
        <td>
          <p class="paragraph">
            [C<sub>6</sub>H<sub>4</sub>NH]<sub>n</sub>
          </p>
        </td>
        <td>
          <p class="paragraph">10<sup>-2</sup>-10<sup>0</sup></p>
        </td>
      </tr>
      <tr>
        <td><p class="paragraph">Polypyrrole</p></td>
        <td><p class="paragraph">PPy</p></td>
        <td>
          <p class="paragraph">
            [C<sub>4</sub>H<sub>2</sub>NH]<sub>n</sub>
          </p>
        </td>
        <td><p class="paragraph">2-100</p></td>
      </tr>
      <tr>
        <td><p class="paragraph">Polythiophene</p></td>
        <td><p class="paragraph">PT</p></td>
        <td>
          <p class="paragraph">[C<sub>4</sub>H<sub>4</sub>S]<sub>n</sub></p>
        </td>
        <td>
          <p class="paragraph">10<sup>0</sup>-10<sup>3</sup></p>
        </td>
      </tr>
      <tr>
        <td><p class="paragraph">Poly(p-phenylene)</p></td>
        <td><p class="paragraph">PPP</p></td>
        <td>
          <p class="paragraph">[C<sub>6</sub>H<sub>4</sub>]<sub>n</sub></p>
        </td>
        <td>
          <p class="paragraph">10<sup>−3</sup> −10<sup>2</sup></p>
        </td>
      </tr>
      <tr>
        <td><p class="paragraph">Polyacetylene</p></td>
        <td><p class="paragraph">PA</p></td>
        <td>
          <p class="paragraph">[C<sub>2</sub>H<sub>2</sub>]<sub>n</sub></p>
        </td>
        <td>
          <p class="paragraph">10<sup>5</sup></p>
        </td>
      </tr>
    </tbody>
    </table>
    <h2>Common types of ECPs</h2>
    <h3>Polyaniline (PANI)</h3>
    <p class="paragraph">
    Polyaniline (PANI) is a highly regarded ECP due to its easy preparation,
    cost-effectiveness, high electrical conductivity, biocompatibility, low
    toxicity, and environmental stability [10]. However, it has certain
    drawbacks, such as low solubility, insolubility in common solvents,
    infusibility, weak processability, and decreasing electrical
    conductivity over time [11]. To address these limitations, various
    techniques have been developed, including re-doping with functionalized
    organic acids, copolymerization with PANI derivatives or other polymers,
    and preparing blends and nanocomposites with various materials [12].
    </p>
    <p class="paragraph">
    PANI nanocomposites are highly promising ECP nanocomposites as they
    combine the PANI matrix with conducting or insulating nanofillers to
    enhance their properties. These nanocomposites have improved properties,
    making them valuable in various fields such as electronics, water
    purification, tissue regeneration, and antimicrobial therapy.
    </p>
    <p class="paragraph">
    PANI or "aniline black" is one of the oldest ECPs, discovered in the
    mid-19<sup>th</sup> century. PANI molecular structure may possess
    benzenoid, quinonoid units, or both types in different proportions [13].
    Chemical and electrochemical oxidative polymerization are the most
    common methods for synthesizing PANIs in an acidic medium. Ammonium
    persulfate and potassium persulfate are the most widely used initiators
    for chemical polymerization. The electrochemical method is preferred for
    small-scale synthesis, while the chemical method allows large-scale
    polymer and nanocomposite preparation [14]. The electrode coating and
    co-deposition approaches are two electrochemical methods used for PANI
    synthesis. PANIs of various nanostructures with different properties
    have been synthesized using several techniques, such as solution,
    self-assembling, heterophase interfacial, and electrochemical
    polymerizations. PANI nanostructures, such as nanospheres, nanogranules,
    nanorods, nanoflowers, nanofibers, and nanotubes, have been synthesized
    using different procedures and parameters, including the initiator or
    oxidant, pH, temperature, solvent, chemical additives, chemical
    oxidation process, template, electrochemistry, radiochemistry, and
    sonochemistry. The electrical properties of PANI-based polymers depend
    on their microscopic and macroscopic properties.
    </p>
    <p class="paragraph">
    PANI is chemically stable and structurally resistant in acidic and
    alkaline solutions, without any degradation or chemical reaction.
    Depending on the redox states, PANI has different solubility in common
    organic solvents. PANI pellets demonstrate superior mechanical strength
    due to the good compactness of the PANI powders. The physical and
    mechanical properties of PANI nanocomposites, such as Young's modulus
    and heat resistance, are significantly improved.
    <strong>Figure 1</strong> displays the various oxidation forms of PANI.
    PANI has shown promise in the field of biomedicine. It possesses
    biocompatibility and can interact with biological systems, making it
    suitable for applications such as drug delivery, tissue engineering
    scaffolds, and biosensors. Polyaniline-based materials have been
    explored for their potential in controlled drug release, cell culture
    substrates, and bioelectrodes, among others [4].
    </p>
    <p class="paragraph">
    However, challenges remain in optimizing and expanding the applications
    of PANI. These include improving its stability under prolonged exposure
    to environmental factors, enhancing its processability for large-scale
    production, and addressing potential issues related to its long-term
    performance and compatibility in specific applications.
    </p>
    <p class="paragraph">
    In summary, PANI is a versatile conducting polymer with excellent
    electrical conductivity, environmental stability, and tunable
    properties. Its broad range of applications spans from electronics and
    energy storage to corrosion protection, biomedical devices, and beyond.
    Ongoing research and development efforts continue to explore and unlock
    the full potential of polyaniline in various fields, driving
    advancements in materials science and technology.
    </p>
    <p class="paragraph"></p>
    <p class="paragraph"></p>
    <figure>
    <img class="fake"
      src="/images/image005.png"
      alt="image005.png"
      data-id="186274ef-765b-48fd-8228-2d9749cdc685"
      data-fileid="cd774ecd-b97e-42e8-ab8c-780adb8384eb"
    />
    <figcaption id="186274ef-765b-48fd-8228-2d9749cdc685" class="decoration">
      <strong>Figure 1</strong>. Different oxidation forms of polyaniline.
      Reprinted with permission from [4]
    </figcaption>
    </figure>
    <p class="paragraph"></p>
    <p class="paragraph"></p>
    <p class="paragraph"></p>
    <p class="paragraph"></p>
    <h2>Polypyrrole (PPy)</h2>
    <p class="paragraph">
    Polypyrrole (PPy) is a type of polymer that has gained attention for its
    excellent electrical conductivity, which makes it a promising candidate
    for electronic applications. The polymer's high conductivity is
    primarily attributed to the polymer backbones that facilitate the easy
    movement of electrons within and between them [15]. PPy has a conjugated
    backbone with alternating single and double bonds with
    sp<sup>2</sup>-hybridized carbon atoms. The electrical conductivity of
    PPy changes from an insulator to a semiconductor, with a band gap &lt;
    2.5 eV, once it is doped [16].
    </p>
    <p class="paragraph">
    &nbsp;The doping process creates polarons and bipolaron species that
    enhance the electrical conductivity of PPy. The exact mechanism of
    charge transfer in a PPy chain has not been fully elucidated, but it is
    believed that polarons and bipolarons are created during the doping
    process [17].
    </p>
    <p class="paragraph">
    Polarons are π-conjugated structures based on aromatic rings that are
    formed from the elimination of an electron from the p system of PPy,
    generating a free radical and a cation [18]. The radical cation then
    undergoes a bond rearrangement to form a quinonoid system. In general,
    species with two charges in the same unit cell are termed a bipolaron,
    regardless of whether the species are stable or not. Doping may be
    performed chemically, electrochemically, or by using photo-doping, and
    organic and inorganic dopants have been used for doping PPy [19]. Small
    and large molecule dopants modulate the electrical conductivity and the
    surface structural properties of PPy in different fashions, with larger
    dopants enhancing the electrochemical stability of the synthesized PPy.
    </p>
    <p class="paragraph">
    Apart from doping, the electrical conductivity of PPy is affected by the
    type of oxidant, the initial oxidant/pyrrole molar ratio, synthesis
    procedure, reaction duration, and temperature. The surfactant
    concentration also plays a significant role in the formation of PPy
    structures with different morphologies. The electrical conductivity of
    PPy synthesized using oxidants such as FeCl<sub>3</sub> or ammonium
    persulfate increases with the temperature of the reaction.[20] The
    electrical conductivity of PPy synthesized using FeCl3 as an oxidant was
    higher than when synthesized with ammonium persulfate as an oxidant,
    both in the presence of an anionic surfactant such as sodium dodecyl
    sulfate. This may be due to the difference in chemical structure between
    the two oxidants, and the interaction of pyrrole with sodium dodecyl
    sulfate during the synthesis [21, 22]. <strong>Figure 2</strong> shows
    the electrochemical and chemical synthesis methods of PPy.
    </p>
    <p class="paragraph">
    PPy exhibits good biocompatibility, which makes it suitable for
    biomedical applications. It can be utilized in drug delivery systems,
    tissue engineering scaffolds, and bioelectrodes due to its ability to
    interface with biological systems. The biocompatible nature of PPy,
    combined with its electrical conductivity, provides opportunities for
    bioelectronic devices and neural interfaces [23].
    </p>
    <p class="paragraph">
    As with any material, there are ongoing research efforts to optimize and
    improve the performance of PPy. Scientists are exploring various
    strategies to enhance its conductivity, tailor its properties, and
    expand its functionality. This includes incorporating different dopants,
    composites, and nanostructures into the PPy matrix to achieve specific
    characteristics and performance enhancements.
    </p>
    <p class="paragraph">
    In conclusion, PPy stands as a highly versatile and intriguing
    conducting polymer, renowned for its exceptional electrical
    conductivity, redox properties, processability, and optical
    characteristics. Its broad range of applications spans from electronics
    and optoelectronics to biomedicine, offering promising avenues for
    technological advancements and innovations.
    </p>
    <p class="paragraph"></p>
    <figure>
    <img class="fake"
      src="/images/image006.jpg"
      alt="image006.jpg"
      data-id="8454b6b9-edb9-4e4b-b23f-1b281185f829"
      data-fileid="c53622cb-2119-4184-9b55-65d8786af9f4"
    />
    <figcaption id="8454b6b9-edb9-4e4b-b23f-1b281185f829" class="decoration">
      <strong>Figure 2</strong>. Electrochemical <strong>(A)</strong>, and
      chemical <strong>(B)</strong> synthesis methods of polypyrrole.
      Reprinted with permission from [24]
    </figcaption>
    </figure>
    <h3>Polythiophene (PT)</h3>
    <p class="paragraph">
    Polythiophene (PT) is a type of conjugated polymer that exhibits stable
    conductivity and high electrical conductivity [5]. PT is nontransparent
    and refractory, but its transparency can be increased by dilution
    through various methods such as block copolymerization, alkyl side chain
    grafting, blending with a transparent polymer, producing composites,
    plasma polymerization, electrochemical procedures, and thin layer PT
    deposition [25].
    </p>
    <p class="paragraph">
    PT has garnered considerable interest in both research and industrial
    sectors due to its exceptional environmental stability, improved thermal
    stability, and reduced band gap energy. These desirable properties,
    coupled with their semiconducting, electronic, and optical activities,
    as well as their superior mechanical characteristics and ease of
    processing, have generated significant attention towards PT composites.
    PT has found extensive application in solar cells due to its ability to
    establish improved contact with metal electrodes and its stability under
    ambient conditions. The PT matrix serves as an efficient polymer for
    transporting holes, making it highly compatible with semiconducting
    particles. This combination creates a new hybrid variety with
    exceptional electrical properties. Furthermore, PT stands out as an
    excellent ECPs, due to the conjugated double bonds in the polymer
    backbone. Similar to other conducting polymers, the conductivity of the
    PT matrix can be enhanced by inducing polarons and bipolarons through
    oxidation or reduction processes. This flexibility allows for the
    utilization of PT in various applications such as polymer batteries,
    electrochromic devices, and solar cells. In summary, the unique
    properties of PT, including its compatibility with metal electrodes,
    stability, hole transport capability, optical transparency, and
    intrinsic conductivity, have made it highly sought-after for use in
    solar cells, electrochromic devices, and polymer batteries. By
    leveraging these distinctive characteristics, PT has demonstrated its
    potential for enhancing electrical and optical performances in these
    technologies. Furthermore, the simplicity of polymerization and the air
    stability of PT makes it even more significant compared to other
    conducting polymers [26, 27].
    </p>
    <h3>Poly(p-phenylene) (PPP)</h3>
    <p class="paragraph">
    Poly(p-phenylene) (PPP) is a conducting polymer that has attracted
    considerable attention due to its excellent electrical conductivity. PPP
    is composed of repeating units of p-phenylene, which consists of benzene
    rings connected by carbon-carbon single bonds. PPP exhibits high
    conductivity primarily due to its extended conjugated structure. The
    delocalized π-electron system along the polymer chain allows for the
    efficient movement of electrons, facilitating electrical conductivity.
    The alternating single and double bonds in the p-phenylene units create
    a favorable pathway for electron delocalization and transport [28].
    </p>
    <p class="paragraph">
    To enhance the conductivity of PPP, various materials have been
    hybridized with it, including zinc oxide nanoparticles, silicate
    platelets, polystyrene mixtures, amino or carboxyl groups,
    poly-L-lysine, and (diphenylamino)-s-triazine. Moreover, the electrical
    conductivity of PPP can be further enhanced by controlling its oxidation
    state. By oxidizing the polymer through chemical or electrochemical
    methods, PPP can undergo doping, resulting in the introduction of
    charged species into the polymer structure. This process, known as
    p-doping, increases the number of charge carriers and thus enhances the
    electrical conductivity of PPP. The conductivity of PPP can also be
    influenced by the presence of impurities, defects, or dopants in the
    polymer matrix. The introduction of dopant molecules can lead to the
    formation of charge transfer complexes, which contribute to enhanced
    electrical conductivity. Techniques such as fluorination and sulfonation
    have also been employed [14, 29, 30].
    </p>
    <p class="paragraph">
    The high conductivity of PPP makes it suitable for various electronic
    and optoelectronic applications. It can be utilized as a conductive
    coating or electrode material, as well as in organic electronic devices
    such as transistors, sensors, and light-emitting diodes (LEDs). PPP's
    conductivity, combined with its good processability and mechanical
    properties, makes it a promising candidate for integrating electronic
    functionalities into flexible and lightweight devices. However, it is
    worth noting that the electrical conductivity of PPP can be influenced
    by factors such as molecular weight, crystallinity, processing
    conditions, and the presence of defects. Researchers are actively
    exploring different strategies to further improve the electrical
    conductivity of PPP and optimize its performance in various applications
    [30-32].
    </p>
    <p class="paragraph">
    In summary, PPP is a conducting polymer with remarkable electrical
    conductivity. Its extended conjugated structure and ability to undergo
    doping enable efficient charge transport. The high conductivity of PPP
    opens up opportunities for its utilization in electronic and
    optoelectronic devices, contributing to advancements in the field of
    organic electronics.
    </p>
    <h3>Polyaniline (PANI)</h3>
    <p class="paragraph">
    Polypyrrole (PPy) is a type of polymer that has gained attention for its
    excellent electrical conductivity, which makes it a promising candidate
    for electronic applications. The polymer’s high conductivity is
    primarily attributed to the polymer backbones that facilitate the easy
    movement of electrons within and between them [15]. PPy has a conjugated
    backbone with alternating single and double bonds with
    sp<sup>2</sup>-hybridized carbon atoms. The electrical conductivity of
    PPy changes from an insulator to a semiconductor, with a band gap &lt;
    2.5 eV, once it is doped [16].
    </p>
    <p class="paragraph">
    The doping process creates polarons and bipolaron species that enhance
    the electrical conductivity of PPy. The exact mechanism of charge
    transfer in a PPy chain has not been fully elucidated, but it is
    believed that polarons and bipolarons are created during the doping
    process [17].
    </p>
    <p class="paragraph">
    Polarons are π-conjugated structures based on aromatic rings that are
    formed from the elimination of an electron from the p system of PPy,
    generating a free radical and a cation [18]. The radical cation then
    undergoes a bond rearrangement to form a quinonoid system. In general,
    species with two charges in the same unit cell are termed a bipolaron,
    regardless of whether the species are stable or not. Doping may be
    performed chemically, electrochemically, or by using photo-doping, and
    organic and inorganic dopants have been used for doping PPy [19]. Small
    and large molecule dopants modulate the electrical conductivity and the
    surface structural properties of PPy in different fashions, with larger
    dopants enhancing the electrochemical stability of the synthesized PPy.
    </p>
    <p class="paragraph">
    Apart from doping, the electrical conductivity of PPy is affected by the
    type of oxidant, the initial oxidant/pyrrole molar ratio, synthesis
    procedure, reaction duration, and temperature. The surfactant
    concentration also plays a significant role in the formation of PPy
    structures with different morphologies. The electrical conductivity of
    PPy synthesized using oxidants such as FeCl<sub>3</sub> or ammonium
    persulfate increases with the temperature of the reaction.[20] The
    electrical conductivity of PPy synthesized using FeCl3 as an oxidant was
    higher than when synthesized with ammonium persulfate as an oxidant,
    both in the presence of an anionic surfactant such as sodium dodecyl
    sulfate. This may be due to the difference in chemical structure between
    the two oxidants, and the interaction of pyrrole with sodium dodecyl
    sulfate during the synthesis [21, 22]. <em>Figure 2</em> shows the
    electrochemical and chemical synthesis methods of PPy.
    </p>
    <p class="paragraph">
    PPy exhibits good biocompatibility, which makes it suitable for
    biomedical applications. It can be utilized in drug delivery systems,
    tissue engineering scaffolds, and bioelectrodes due to its ability to
    interface with biological systems. The biocompatible nature of PPy,
    combined with its electrical conductivity, provides opportunities for
    bioelectronic devices and neural interfaces [23].
    </p>
    <p class="paragraph">
    As with any material, there are ongoing research efforts to optimize and
    improve the performance of PPy. Scientists are exploring various
    strategies to enhance its conductivity, tailor its properties, and
    expand its functionality. This includes incorporating different dopants,
    composites, and nanostructures into the PPy matrix to achieve specific
    characteristics and performance enhancements.
    </p>
    <p class="paragraph">
    In conclusion, PPy stands as a highly versatile and intriguing
    conducting polymer, renowned for its exceptional electrical
    conductivity, redox properties, processability, and optical
    characteristics. Its broad range of applications spans from electronics
    and optoelectronics to biomedicine, offering promising avenues for
    technological advancements and innovations.
    </p>
    <figure>
    <img class="fake"
      src="/images/image007.png"
      alt="image007.png"
      data-id=""
      data-fileid="42c4d549-cffb-4a2d-ad0a-fe1341299301"
    />
    <figcaption id="" class="decoration">
      <strong>Figure 2</strong>. Electrochemical <strong>(A)</strong>, and
      chemical <strong>(B)</strong> synthesis methods of polypyrrole.
      Reprinted with permission from [24]
    </figcaption>
    </figure>
    <p class="paragraph"></p>
    <h3>Polythiophene (PT)</h3>
    <p class="paragraph">
    Polythiophene (PT) is a type of conjugated polymer that exhibits stable
    conductivity and high electrical conductivity [5]. PT is nontransparent
    and refractory, but its transparency can be increased by dilution
    through various methods such as block copolymerization, alkyl side chain
    grafting, blending with a transparent polymer, producing composites,
    plasma polymerization, electrochemical procedures, and thin layer PT
    deposition [25].
    </p>
    <p class="paragraph">
    PT has garnered considerable interest in both research and industrial
    sectors due to its exceptional environmental stability, improved thermal
    stability, and reduced band gap energy. These desirable properties,
    coupled with their semiconducting, electronic, and optical activities,
    as well as their superior mechanical characteristics and ease of
    processing, have generated significant attention towards PT composites.
    PT has found extensive application in solar cells due to its ability to
    establish improved contact with metal electrodes and its stability under
    ambient conditions. The PT matrix serves as an efficient polymer for
    transporting holes, making it highly compatible with semiconducting
    particles. This combination creates a new hybrid variety with
    exceptional electrical properties. Furthermore, PT stands out as an
    excellent ECPs, due to the conjugated double bonds in the polymer
    backbone. Similar to other conducting polymers, the conductivity of the
    PT matrix can be enhanced by inducing polarons and bipolarons through
    oxidation or reduction processes. This flexibility allows for the
    utilization of PT in various applications such as polymer batteries,
    electrochromic devices, and solar cells. In summary, the unique
    properties of PT, including its compatibility with metal electrodes,
    stability, hole transport capability, optical transparency, and
    intrinsic conductivity, have made it highly sought-after for use in
    solar cells, electrochromic devices, and polymer batteries. By
    leveraging these distinctive characteristics, PT has demonstrated its
    potential for enhancing electrical and optical performances in these
    technologies. Furthermore, the simplicity of polymerization and the air
    stability of PT makes it even more significant compared to other
    conducting polymers [26, 27].
    </p>
    <h3>Poly(p-phenylene) (PPP)</h3>
    <p class="paragraph">
    Poly(p-phenylene) (PPP) is a conducting polymer that has attracted
    considerable attention due to its excellent electrical conductivity. PPP
    is composed of repeating units of p-phenylene, which consists of benzene
    rings connected by carbon-carbon single bonds. PPP exhibits high
    conductivity primarily due to its extended conjugated structure. The
    delocalized π-electron system along the polymer chain allows for the
    efficient movement of electrons, facilitating electrical conductivity.
    The alternating single and double bonds in the p-phenylene units create
    a favorable pathway for electron delocalization and transport [28].
    </p>
    <p class="paragraph">
    To enhance the conductivity of PPP, various materials have been
    hybridized with it, including zinc oxide nanoparticles, silicate
    platelets, polystyrene mixtures, amino or carboxyl groups,
    poly-L-lysine, and (diphenylamino)-s-triazine. Moreover, the electrical
    conductivity of PPP can be further enhanced by controlling its oxidation
    state. By oxidizing the polymer through chemical or electrochemical
    methods, PPP can undergo doping, resulting in the introduction of
    charged species into the polymer structure. This process, known as
    p-doping, increases the number of charge carriers and thus enhances the
    electrical conductivity of PPP. The conductivity of PPP can also be
    influenced by the presence of impurities, defects, or dopants in the
    polymer matrix. The introduction of dopant molecules can lead to the
    formation of charge transfer complexes, which contribute to enhanced
    electrical conductivity. Techniques such as fluorination and sulfonation
    have also been employed [14, 29, 30].
    </p>
    <p class="paragraph">
    The high conductivity of PPP makes it suitable for various electronic
    and optoelectronic applications. It can be utilized as a conductive
    coating or electrode material, as well as in organic electronic devices
    such as transistors, sensors, and light-emitting diodes (LEDs). PPP’s
    conductivity, combined with its good processability and mechanical
    properties, makes it a promising candidate for integrating electronic
    functionalities into flexible and lightweight devices. However, it is
    worth noting that the electrical conductivity of PPP can be influenced
    by factors such as molecular weight, crystallinity, processing
    conditions, and the presence of defects. Researchers are actively
    exploring different strategies to further improve the electrical
    conductivity of PPP and optimize its performance in various applications
    [30–32].
    </p>
    <p class="paragraph">
    In summary, PPP is a conducting polymer with remarkable electrical
    conductivity. Its extended conjugated structure and ability to undergo
    doping enable efficient charge transport. The high conductivity of PPP
    opens up opportunities for its utilization in electronic and
    optoelectronic devices, contributing to advancements in the field of
    organic electronics.
    </p>
    <h3>Polyacetylene (PA)</h3>
    <p class="paragraph">
    Polyacetylene (PA) is a macromolecule that has garnered much attention
    in the scientific community due to its unique and multifaceted
    properties. It is a conjugated polymer that displays electrical
    conductivity, photoconductivity, gas permeability, supramolecular
    assemblies, chiral recognition, helical graphitic nanofiber formation,
    and liquid crystal behavior. The chemical structure of PA is a linear
    polyene chain, where the backbone provides a platform for decoration
    with pendants. The presence of repeated units of two hydrogen atoms in
    the backbone allows for the substitution of hydrogen with one or two
    substitutes, resulting in monosubstituted or disubstituted PAs,
    respectively (<em>Figure 3).</em>. PA can be synthesized using various
    methods. One of the methods is Ziegler-Natta catalysis, which involves
    the use of titanium and aluminum in the presence of gaseous acetylene
    [33]. By adjusting the temperature and amount of catalyst, PA can be
    synthesized while monitoring the structure and final polymer products.
    Alternative methods of polymerization radiations, such as glow
    discharge, ultraviolet, and Y-radiation, could also be used to
    synthesize PA, which eliminates the use of catalysts and solvents [34].
    </p>
    <figure>
    <img class="fake"
      src="https://kotahi-s3.cloud68.co/kotahi-universcipress/69f7b84c9124_medium.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=zlqRHjmWNyrcCrrAKvOPjuwuBGgmxE%2F20231206%2Fus-east-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20231206T101606Z&amp;X-Amz-Expires=86400&amp;X-Amz-Signature=de445e3b3d207b9d68bbb1fe9c73a0d9dc94669b8cf0f3920f4b9c46d388e90e&amp;X-Amz-SignedHeaders=host"
      alt="Image2.png"
      data-id=""
      data-fileid="6369a2e5-3a0b-4582-8a45-91df2af65821"
    />
    <figcaption id="" class="decoration">
      <em>Figure 3.</em> Mechanism of electrical conductivity of
      polyacetylene. Reprinted with permission from [35]
    </figcaption>
    </figure>
    <h3>Properties and applications of ECPs</h3>
    <p class="paragraph">
    The structure and properties of ECPs play a crucial role in determining
    their performance in biomedical applications. ECPs typically possess
    alternating single and double bonds, which form π-conjugated systems
    that give them unique properties in electrical and electrochemical
    applications. The presence of π-conjugated systems within the polymer
    structure allows for the delocalization of electrons, resulting in
    electrical conductivity. The extent of conjugation and the degree of
    doping heavily influence the conductivity of the polymer. Higher
    conductivity is desirable for applications such as electrodes,
    biosensors, and neural interfaces, where efficient electrical
    communication is required [30, 36–38].
    </p>
    <p class="paragraph">
    The most significant factors that influence ECPs are their degree of
    crystallinity, conjugation length, and interactions within and between
    chains. These aspects have been identified as having the most
    significant impact on the properties of ECPs [39]. Moreover, the
    mechanical properties of ECPs, including flexibility, elasticity, and
    strength, determine their ability to withstand deformations and
    mechanical stresses. Flexible and stretchable polymers are desirable for
    applications in wearable devices, implantable electronics, and tissue
    engineering scaffolds to ensure compatibility with the surrounding
    tissues and enhance long-term performance [40, 41].
    </p>
    <p class="paragraph">
    The electrical conductivity of a substance is determined by its
    structure, which can be categorized into three types: conductive,
    semiconductive, and insulating. The energy band theory is useful in
    classifying materials based on their electrical properties [42].
    Conductive materials have an overlapping valence and conduction band,
    allowing electrons to move freely in the conduction band. In
    semiconductors, the energy gap between the valence and conduction bands
    is small, allowing for intrinsic electron transfer with low energy.
    Insulators have a large energy gap between the two bands, making it
    difficult for electrons to move through the material. However, these
    categories do not necessarily apply to ECPs, which require
    molecular-level studies to determine their conductivity [18, 43]. ECPs
    achieve electrical conductivity through the sharing or propagation of
    charges along their backbone, which consists of alternating single and
    double bonds. This creates an unpaired π electron per carbon atom, and
    the sp2pz hybrid configuration of carbon atoms allows for delocalized
    electrons within the polymer chain. This results in charge mobility
    throughout the polymer structure, enabling features such as electrical
    conductivity, low energy light transmission, and low ionization
    potential [44].
    </p>
    <p class="paragraph">
    The presence of both single and double bonds, including σ and π-bonds,
    allows for electron flow and electrical conductivity in ECPs [30, 45].
    To enhance their electrical conductivity, dopant materials can be
    introduced to these polymers. The addition of various chemical species
    during the polymerization or synthesis process can lead to the formation
    of nonlinear defects such as solitons, poles, dipoles, and electrical
    contributions [46, 47]. These dopant materials can be adjusted to
    control the density and mobility of charge carriers.
    </p>
    <p class="paragraph">
    The electrical conductivity of polymers is dependent on several factors,
    including the degree of doping, the arrangement of the polymer chains,
    the conjugation length of the polymer composition, and the purity of the
    samples. It should also be noted that the methods used to produce high
    conductivity in polymers and inorganic semiconductors differ, and thus
    different conductivities can be achieved depending on the percentage of
    doping species present in the degraded polymers [47].
    </p>
    <p class="paragraph">
    To gain a deeper understanding of the electronic structure of ECPs, it
    is essential to investigate their optical properties. The
    spectrophotometric spectrum can reveal the color and electron spectrum
    of polymers, particularly when associated with other compounds such as
    dopants. The variation of optical spectra during the doping process is
    crucial as it can provide insights into the nature and mechanism of
    charged species in the polymer chain [48].
    </p>
    <p class="paragraph">
    Conjugated polymers have large and rapid nonlinear light response
    properties that can be fine-tuned through adjustable solution
    processing. The degree of electron localization (conjugation) and the
    combination of transition metals with polarizable electrons can increase
    the hyperpolarizability of materials. The nonlinear optical response of
    conjugated polymers depends on several factors, including the conjugate
    length, the orientation of the polymer skeleton, and the relative
    orientation and structural properties of the chromophore complex and
    Schiff base metal complexes. The presence of π-bonds in the skeleton of
    conjugated polymers enables photon-electron interaction and shows an
    anisotropic electron or quasi-one-dimensional structure [49].
    </p>
    <p class="paragraph">
    The electronic behavior of organic semiconductors is influenced by the
    effect of charge stimuli, such as solitons, polarons, or bipolarons in
    the ground state degeneracy. In conjugated polymeric skeletons, the
    optical transitions occur due to the release of charge from dopants or
    from the transfer of oscillator strength from π to π*. Conjugated
    polymers exhibit behavior similar to semiconductors in their pristine
    state and behave like metals when doped with dopants (n-type or p-type)
    [33].
    </p>
    <p class="paragraph">
    ECPs have gained significant attention in the biomedical sector due to
    their unique properties and advantages over traditional materials
    including: (i) Low cost: ECPs can often be synthesized using relatively
    inexpensive methods and materials, which can lead to cost-effective
    production compared to traditional materials like metals or inorganic
    semiconductors. This advantage contributes to the potential for
    widespread adoption of ECPs in the biomedical sector [50–52]; (ii) Low
    detection limit: ECPs can be used to prepare biosensors that enable
    real-time, point-of-care testing. These devices have a low detection
    limit, which can ultimately result in increased access to treatment
    [50]; (iii) Chemical stability: Polymeric materials possess chemical
    stability, which makes them more durable and long-lasting [48]; (iv)
    Good processability: ECPs can be processed using various techniques,
    including solution casting, 3D printing, and electrospinning. This
    processability allows for the fabrication of complex shapes, structures,
    and patterns, making them suitable for applications that require precise
    geometries, such as tissue engineering scaffolds or microfluidic devices
    [48, 53]; (v) Tunable electrical properties: ECPs have tunable
    electrical properties, which means they can be tailored to meet specific
    requirements for different biomedical applications. This property allows
    for the development of electronic components, such as sensors,
    electrodes, and actuators, which are crucial for many biomedical
    applications [30]; (vi) Stimuli Responsiveness: Some ECPs exhibit
    stimuli responsiveness, meaning their electrical or mechanical
    properties can be altered in response to external stimuli, such as
    temperature, pH, or light. This property can be harnessed for
    applications like drug delivery systems, where controlled release of
    medication can be achieved by applying a specific stimulus [40, 52].
    </p>
    <p class="paragraph">
    The development of ECPs for biomedical applications is accompanied by
    several challenges. Ensuring biocompatibility is a significant challenge
    for ECPs. Some polymers may exhibit inherent cytotoxicity or cause
    adverse reactions when in contact with living tissues. Modifying ECPs
    can be done in several ways to enhance their biocompatibility and reduce
    toxicity: (i) Surface Modification: Modifying the surface of ECPs is a
    common approach to improving their biocompatibility. This can be
    achieved by functionalizing the polymer surface with bioactive
    molecules, such as peptides, proteins, or carbohydrates. These bioactive
    moieties can promote cell adhesion, prevent protein adsorption, and
    modulate the immune response, leading to enhanced biocompatibility [40,
    41, 54]; (ii) Polymer Blending: Blending ECPs with biocompatible
    polymers can result in hybrid materials with improved biocompatibility.
    By combining the advantageous properties of both polymers, such as
    electrical conductivity and biocompatibility, the resulting blend can
    exhibit enhanced performance in biomedical applications. The choice of
    the biocompatible polymer and the blending ratio should be carefully
    optimized to achieve the desired properties [54–56]; (iii)
    Preconditioning and Surface Treatments: Preconditioning techniques such
    as sterilization, plasma treatment, or UV irradiation can modify the
    surface properties of ECPs, improving their biocompatibility. These
    treatments can alter surface chemistry, charge, or roughness, thereby
    reducing toxicity and promoting favorable cellular responses [57–60].
    </p>
    <p class="paragraph">
    Moreover, ECPs may be susceptible to degradation, especially under
    physiological conditions or in the presence of reactive species. This
    can lead to a loss of electrical conductivity or structural integrity
    over time. To address this challenge, researchers can explore strategies
    such as polymer synthesis optimization, crosslinking, encapsulation, or
    the use of protective coatings to enhance stability and resistance to
    degradation [40, 41, 54, 59].
    </p>
    <p class="paragraph">
    While ECPs may possess good electrical conductivity, their mechanical
    properties, such as strength, flexibility, or stretchability, may need
    improvement to meet the demands of biomedical applications. Strategies
    like polymer blending with elastomers or reinforcement with nanofillers
    can enhance the mechanical properties and tailor them to specific
    application requirements [40, 41, 51, 55, 61].
    </p>
    <p class="paragraph">
    The long-term performance and stability of ECPs in the body are also
    essential for successful biomedical applications. Factors such as
    biostability, resistance to fatigue, and biodegradation (if desired)
    need to be carefully considered and addressed during the design phase.
    Long-term in vivo studies can provide insights into the behavior of ECPs
    over extended periods and help identify potential challenges and
    necessary improvements [59, 62, 63].
    </p>
    <h2>Future perspective of ECPs in the biomedical sector</h2>
    <p class="paragraph">
    The future of ECPs in the biomedical sector holds great promise. As
    research and development efforts continue, we can expect significant
    advancements and the emergence of new applications. Here are some
    potential areas where ECPs could have a notable impact in the near
    future: (i) Implantable Medical Devices: ECPs can revolutionize the
    field of implantable medical devices, such as neural interfaces,
    bioelectrodes, and cardiac devices. These polymers can provide enhanced
    biocompatibility, flexibility, and conductivity, enabling improved
    long-term performance and biointegration. They can facilitate better
    communication with the body’s tissues and nerves, allowing for more
    precise control and feedback in therapeutic interventions [64–66]; (ii)
    Tissue Engineering and Regenerative Medicine: ECPs have the potential to
    play a critical role in tissue engineering and regenerative medicine. By
    incorporating electroconductive scaffolds or hydrogels, these polymers
    can provide electrical cues to guide cell behavior and enhance tissue
    regeneration. They can aid in the development of functional engineered
    tissues, such as cardiac patches, neural grafts, or muscle constructs,
    with the ability to integrate and communicate with the surrounding host
    tissue [40, 54, 59, 67]; (iii) Wearable and Flexible Electronics: The
    field of wearable electronics can benefit significantly from the
    properties of ECPs. These polymers can enable the development of
    flexible, stretchable, and conformable electronic devices that can be
    comfortably worn on the body. Applications could include smart textiles,
    wearable biosensors for health monitoring, electronic skin for
    prosthetics, and advanced human-machine interfaces [68–71]; (iv)
    Controlled Drug Delivery Systems: ECPs can be utilized in the
    development of controlled drug delivery systems. By incorporating drugs
    or therapeutic agents within the polymer matrix, they can enable
    controlled and localized release based on external stimuli or specific
    physiological conditions. These systems can improve the efficacy and
    safety of drug delivery, particularly in areas such as cancer therapy,
    chronic pain management, or wound healing [72–75]; (v) Bioelectronics
    and Bioresponsive Interfaces: ECPs can be employed in the development of
    bioelectronic interfaces that bridge the gap between biological systems
    and electronic devices. They can enable direct communication and signal
    transduction with biological entities, such as neurons or cells, for
    applications in neuroprosthetics, bioelectronic implants, or
    brain-computer interfaces. The unique properties of ECPs can facilitate
    the development of bioresponsive interfaces that can sense and adapt to
    dynamic physiological conditions [54, 55, 59, 61]; (vi) Biosensors and
    Diagnostics: ECPs can be utilized in the development of biosensors and
    diagnostic devices for various healthcare applications. These polymers
    can serve as transducing elements in sensors, enabling the detection and
    quantification of biomarkers or analytes. Their electrical conductivity
    can be modulated by the presence of specific molecules, facilitating
    highly sensitive and selective detection, potentially leading to
    advancements in point-of-care diagnostics and personalized medicine [40,
    55, 61, 76].
    </p>
    <p class="paragraph">
    It is important to note that while these potential applications hold
    promise, further research, development, and validation are necessary to
    ensure the efficacy, safety, and reliability of ECPs in real-world
    biomedical settings. Nonetheless, the unique properties of ECPs make
    them a highly promising class of materials with the potential to
    revolutionize various aspects of healthcare and improve patient
    outcomes.
    </p>
    <h2>Conclusions</h2>
    <p class="paragraph">
    In conclusion, ECPs are emerging as a fascinating class of materials
    with a unique set of properties, extending their appeal to a broad
    spectrum of applications. This review serves as a comprehensive resource
    encompassing the synthesis, characteristics, and implementation of some
    of the most extensively studied ECPs, namely PANI, PPy, PT, PPP, and PA.
    The discourse underlines the significance of comprehending the intrinsic
    link between the structure and properties of ECPs, whilst considering
    how varying factors such as dopants, polymerization techniques, and
    processing conditions can alter their attributes. Furthermore, the
    review furnishes an exhaustive insight into the application of ECPs in
    diverse biomedical sectors, from biosensors to tissue engineering,
    highlighting recent strides and existing challenges in these fields. As
    the realm of ECP research continues to expand, upcoming endeavors should
    concentrate on devising innovative materials and fabrication
    methodologies for sophisticated applications. Given their potential to
    dramatically reshape sectors including electronics, energy, and
    healthcare, ECPs constitute a compelling and pivotal field within
    materials science. Anticipation surrounds the future of ECPs, as they
    are poised to continue drawing considerable interest in the foreseeable
    future.
    </p>
    <h2>Authors’ contributions</h2>
    <p class="paragraph">
    All authors contributed to drafting, and revising of the paper and
    agreed to be responsible for all the aspects of this work.
    </p>
    <h2>Declaration of competing interest</h2>
    <p class="paragraph">The authors declare no competing interest.</p>
    <h2>Funding</h2>
    <p class="paragraph">This paper received no external funding.</p>
    <h2>Data availability</h2>
    <p class="paragraph">Not applicable.</p>
    <h2>References</h2>
    <p class="paragraph">
    [1] D. Ghosh, D. Khastgir, Degradation and Stability of Polymeric
    High-Voltage Insulators and Prediction of Their Service Life through
    Environmental and Accelerated Aging Processes, ACS Omega 3 (2018)
    11317–11330.
    </p>
    <p class="paragraph">
    [2] M.&nbsp;Z. Saleem, M. Akbar, Review of the Performance of
    High-Voltage Composite Insulators, Polymers 14 (2022) 431.
    </p>
    <p class="paragraph">
    [3] T. Nezakati, A. Seifalian, A. Tan, A.M. Seifalian, Conductive
    Polymers: Opportunities and Challenges in Biomedical Applications, Chem.
    Rev. 118 (2018) 6766–6843.
    </p>
    <p class="paragraph">
    [4] E.&nbsp;N. Zare, P. Makvandi, B. Ashtari, F. Rossi, A. Motahari, G.
    Perale, Progress in Conductive Polyaniline-Based Nanocomposites for
    Biomedical Applications: A Review, J. Med. Chem. 63 (2020) 1–22.
    </p>
    <p class="paragraph">
    [5] M. Shanmugam, A. Augustin, S. Mohan, B. Honnappa, C. Chuaicham, S.
    Rajendran, T.&nbsp;K.&nbsp;A. Hoang, K. Sasaki, K. Sekar, Conducting
    polymeric nanocomposites: A review in solar fuel applications, Fuel 325
    (2022) 124899.
    </p>
    <p class="paragraph">
    [6] B. Guo, P.&nbsp;X. Ma, Conducting Polymers for Tissue Engineering,
    Biomacromolecules 19 (2018) 1764–1782.
    </p>
    <p class="paragraph">
    [7] M. Baghayeri, E.&nbsp;N. Zare, M.&nbsp;M. Lakouraj, Monitoring of
    hydrogen peroxide using a glassy carbon electrode modified with
    hemoglobin and a polypyrrole-based nanocomposite, Microchim. Acta 182
    (2015) 771–779.
    </p>
    <p class="paragraph">
    [8] M. Baghayeri, E. Nazarzadeh Zare, M. Mansour Lakouraj, A simple
    hydrogen peroxide biosensor based on a novel electro-magnetic
    poly(p-phenylenediamine)@Fe3O4 nanocomposite, Biosens. Bioelectron. 55
    (2014) 259–265.
    </p>
    <p class="paragraph">
    [9] M. Ghovvati, M. Kharaziha, R. Ardehali, N. Annabi, Recent Advances
    in Designing Electroconductive Biomaterials for Cardiac Tissue
    Engineering, Adv. Healthc. Mater. 11 (2022) 2200055.
    </p>
    <p class="paragraph">
    [10] S. Sarkar, N. Levi-Polyachenko, Conjugated polymer nano-systems for
    hyperthermia, imaging and drug delivery, Adv. Drug Deliv. Rev. 163–164
    (2020) 40–64.
    </p>
    <p class="paragraph">
    [11] A.&nbsp;N. Gheymasi, Y. Rajabi, E.&nbsp;N. Zare, Nonlinear optical
    properties of poly(aniline-co-pyrrole)@ZnO-based nanofluid, Opt. Mater.
    102 (2020) 109835.
    </p>
    <p class="paragraph">
    [12] E.&nbsp;N. Zare, M.&nbsp;M. Lakouraj, Biodegradable
    polyaniline/dextrin conductive nanocomposites: synthesis,
    characterization, and study of antioxidant activity and sorption of
    heavy metal ions, Iran. Polym. J. 23 (2014) 257–266.
    </p>
    <p class="paragraph">
    [13] P. Najafi Moghadam, E. Nazarzadeh Zareh, Synthesis of conductive
    nanocomposites based on polyaniline/poly(styrene-alt-maleic
    anhydride)/polystyrene, e-Polymers 10 (2010).
    </p>
    <p class="paragraph">
    [14] M. Ghomi, E.&nbsp;N. Zare, R.&nbsp;S. Varma, Preparation of
    Conducting Polymers/Composites, Conductive Polymers in Analytical
    Chemistry, ACS (2022) 67–90.
    </p>
    <p class="paragraph">
    [15] M. Gizdavic-Nikolaidis, J. Travas-Sejdic, G.&nbsp;A. Bowmaker,
    R.&nbsp;P. Cooney, C. Thompson, P.&nbsp;A. Kilmartin, The antioxidant
    activity of conducting polymers in biomedical applications, Curr. Appl.
    Phys. 4 (2004) 347–350.
    </p>
    <p class="paragraph">
    [16] P. Jayamurugan, V. Ponnuswamy, S. Ashokan, Y.&nbsp;V.&nbsp;S. Rao,
    T. Mahalingam, PPy Doped with DBSA and Combined with PSS to Improve
    Processability and Control the Morphology, Int. Polym. Process. 30
    (2015) 422–427.
    </p>
    <p class="paragraph">
    [17] H. Yu, Y. He, H. Li, Z. Li, B. Ren, G. Chen, X. Hu, T. Tang, Y.
    Cheng, J.&nbsp;Z. Ou, Core-shell PPy@TiO2 enable GO membranes with
    controllable and stable dye desalination properties, Desalination 526
    (2022) 115523.
    </p>
    <p class="paragraph">
    [18] J. Nightingale, J. Wade, D. Moia, J. Nelson, J.-S. Kim, Impact of
    Molecular Order on Polaron Formation in Conjugated Polymers, J. Phys.
    Chem. C 122 (2018) 29129–29140.
    </p>
    <p class="paragraph">
    [19] M. Gosh, A. Barman, A.&nbsp;K. Meikap, S.&nbsp;K. De, S.
    Chatterjee, Hopping transport in HCl doped conducting polyaniline, Phys.
    Lett. A 260 (1999) 138–148.
    </p>
    <p class="paragraph">
    [20] A. Yussuf, M. Al-Saleh, S. Al-Enezi, G. Abraham, Synthesis and
    Characterization of Conductive Polypyrrole: The Influence of the
    Oxidants and Monomer on the Electrical, Thermal, and Morphological
    Properties, Int. J. Polym. Sci. 2018 (2018) 4191747.
    </p>
    <p class="paragraph">
    [21] H. Masuda, D.&nbsp;K. Asano, Preparation and properties of
    polypyrrole, Synth. Met. 135–136 (2003) 43–44.
    </p>
    <p class="paragraph">
    [22] E. Nazarzadeh Zare, M. Mansour Lakouraj, M. Mohseni, Biodegradable
    polypyrrole/dextrin conductive nanocomposite: Synthesis,
    characterization, antioxidant and antibacterial activity, Synth. Met.
    187 (2014) 9–16.
    </p>
    <p class="paragraph">
    [23] A. Fahlgren, C. Bratengeier, A. Gelmi, C.&nbsp;M. Semeins, J.
    Klein-Nulend, E.&nbsp;W. Jager, A.D. Bakker, Biocompatibility of
    Polypyrrole with Human Primary Osteoblasts and the Effect of Dopants,
    PLoS One 10 (2015) e0134023.
    </p>
    <p class="paragraph">
    [24] E.&nbsp;N. Zare, T. Agarwal, A. Zarepour, F. Pinelli, A. Zarrabi,
    F. Rossi, M. Ashrafizadeh, A. Maleki, M.-A. Shahbazi, T.&nbsp;K. Maiti,
    R.&nbsp;S. Varma, F.&nbsp;R. Tay, M.&nbsp;R. Hamblin, V. Mattoli, P.
    Makvandi, Electroconductive multi-functional polypyrrole composites for
    biomedical applications, Appl. Mater. Today 24 (2021) 101117.
    </p>
    <p class="paragraph">
    [25] A.&nbsp;L. Gomes, M.&nbsp;B. Pinto Zakia, J.&nbsp;G. Filho, E.
    Armelin, C. Alemán, J. Sinezio de Carvalho Campos, Preparation and
    characterization of semiconducting polymeric blends. Photochemical
    synthesis of poly(3-alkylthiophenes) using host microporous matrices of
    poly(vinylidene fluoride), Polym. Chem. 3 (2012) 1334–1343.
    </p>
    <p class="paragraph">
    [26] M.&nbsp;T. Ramesan, K. Suhailath, 13 - Role of nanoparticles on
    polymer composites, in: R.&nbsp;K. Mishra, S. Thomas, N. Kalarikkal
    (Eds.), Micro and Nano Fibrillar Composites (MFCs and NFCs) from Polymer
    Blends, Woodhead Publishing (2017) 301–326.
    </p>
    <p class="paragraph">
    [27] R.&nbsp;P. Pant, S.&nbsp;K. Dhawan, D. Suri, M. Arora, S.&nbsp;K.
    Gupta, M. Koneracká, P. Kopčanský, M. Timko, Synthesis and
    characterization of ferrofluid-conducting polymer composite, Indian J.
    Eng. Mater. Sci. 11 (2004) 267–270.
    </p>
    <p class="paragraph">
    [28] J. Heinze, Electrochemistry of conducting polymers, Synth. Met. 43
    (1991) 2805–2823.
    </p>
    <p class="paragraph">
    [29] S. Tokito, T. Tsutsui, S. Saito, Electrical Conductivity and
    Optical Properties of Poly(p-phenylene sulflde) Doped with Some Organic
    Acceptors, Polym. J. 17 (1985) 959–968.
    </p>
    <p class="paragraph">
    [30] N. K, C.&nbsp;S. Rout, Conducting polymers: a comprehensive review
    on recent advances in synthesis, properties and applications, RSC Adv.
    11 (2021) 5659–5697.
    </p>
    <p class="paragraph">
    [31] H.&nbsp;W. Hill Jr., D.&nbsp;G. Brady, Properties, environmental
    stability, and molding characteristics of polyphenylene sulfide, Polym.
    Eng. Sci. 16 (1976) 831–835.
    </p>
    <p class="paragraph">
    [32] A.&nbsp;S. Rahate, K.&nbsp;R. Nemade, S.&nbsp;A. Waghuley,
    Polyphenylene sulfide (PPS): state of the art and applications, Rev.
    Chem. Eng. 29 (2013) 471–489.
    </p>
    <p class="paragraph">
    [33] D. Yaron, Nonlinear optical response of conjugated polymers:
    Essential excitations and scattering, Phys. Rev. B 54 (1996) 4609–4620.
    </p>
    <p class="paragraph">
    [34] S.&nbsp;H. Cho, K.&nbsp;T. Song, J.&nbsp;Y. Lee, Recent advances in
    polypyrrole (2007) 1–8.
    </p>
    <p class="paragraph">
    [35] A.&nbsp;A. Baleg, M. Masikini, S.&nbsp;V. John, A.&nbsp;R.
    Williams, N. Jahed, P. Baker, E. Iwuoha, Conducting Polymers and
    Composites, in: M.&nbsp;A. Jafar Mazumder, H. Sheardown, A. Al-Ahmed
    (Eds.), Functional Polymers, Springer, Cham. (2018) 1–54.
    </p>
    <p class="paragraph">
    [36] T.-H. Le, Y. Kim, H. Yoon, Electrical and Electrochemical
    Properties of Conducting Polymers, Polymers 9 (2017) 150.
    </p>
    <p class="paragraph">
    [37] S.&nbsp;B. Mdluli, M.&nbsp;E. Ramoroka, S.&nbsp;T. Yussuf,
    K.&nbsp;D. Modibane, V.&nbsp;S. John-Denk, E.&nbsp;I. Iwuoha,
    &amp;pi;-Conjugated Polymers and Their Application in Organic and Hybrid
    Organic-Silicon Solar Cells, Polymers 14 (2022) 716.
    </p>
    <p class="paragraph">
    [38] F. Borrmann, T. Tsuda, O. Guskova, N. Kiriy, C. Hoffmann, D.
    Neusser, S. Ludwigs, U. Lappan, F. Simon, M. Geisler, B. Debnath, Y.
    Krupskaya, M. Al-Hussein, A. Kiriy, Charge-Compensated N-Doped
    π-Conjugated Polymers: Toward both Thermodynamic Stability of N-Doped
    States in Water and High Electron Conductivity, Adv. Sci. 9 (2022)
    2203530.
    </p>
    <p class="paragraph">
    [39] J. Stejskal, M. Trchová, Conducting polypyrrole nanotubes: a
    review, Chem. Pap. 72 (2018) 1563–1595.
    </p>
    <p class="paragraph">
    [40] G. Kaur, R. Adhikari, P. Cass, M. Bown, P. Gunatillake,
    Electrically conductive polymers and composites for biomedical
    applications, RSC Adv. 5 (2015) 37553–37567.
    </p>
    <p class="paragraph">
    [41] H. He, L. Zhang, X. Guan, H. Cheng, X. Liu, S. Yu, J. Wei, J.
    Ouyang, Biocompatible Conductive Polymers with High Conductivity and
    High Stretchability, ACS. Appl. Mater. Interfaces 11 (2019) 26185–26193.
    </p>
    <p class="paragraph">
    [42] T. Pal, S. Banerjee, P.&nbsp;K. Manna, K.&nbsp;K. Kar,
    Characteristics of Conducting Polymers, in: K.&nbsp;K. Kar (Ed.),
    Handbook of Nanocomposite Supercapacitor Materials I: Characteristics,
    Springer, Cham. (2020) 247–268.
    </p>
    <p class="paragraph">
    [43] G. Magela e Silva, Electric-field effects on the competition
    between polarons and bipolarons in conjugated polymers, Phys. Rev. B 61
    (2000) 10777–10781.
    </p>
    <p class="paragraph">
    [44] A.M.&nbsp;R. Ramírez, M.&nbsp;A. Gacitúa, E. Ortega, F.&nbsp;R.
    Díaz, M.&nbsp;A. del Valle, Electrochemical in situ synthesis of
    polypyrrole nanowires, Electrochem. Commun. 102 (2019) 94–98.
    </p>
    <p class="paragraph">
    [45] R. Ravichandran, S. Sundarrajan, J.&nbsp;R. Venugopal, S.
    Mukherjee, S. Ramakrishna, Applications of conducting polymers and their
    issues in biomedical engineering, J.&nbsp;R. Soc. Interface 7(suppl_5)
    (2010) 559–579.
    </p>
    <p class="paragraph">
    [46] B.&nbsp;D. Malhotra, A. Chaubey, S.&nbsp;P. Singh, Prospects of
    conducting polymers in biosensors, Anal. Chim. Acta 578 (2006) 59–74.
    </p>
    <p class="paragraph">
    [47] K.-H. Yang, Y.-C. Liu, C.-C. Yu, Temperature effect of
    electrochemically roughened gold substrates on polymerization
    electrocatalysis of polypyrrole, Anal. Chim. Acta 631 (2009) 40–46.
    </p>
    <p class="paragraph">
    [48] Y. Park, J. Jung, M. Chang, Research Progress on Conducting
    Polymer-Based Biomedical Applications, App. Sci. 9 (2019) 1070.
    </p>
    <p class="paragraph">
    [49] A. Pietrangelo, B.C. Sih, B.&nbsp;N. Boden, Z. Wang, Q. Li,
    K.&nbsp;C. Chou, M.&nbsp;J. MacLachlan, M.&nbsp;O. Wolf, Nonlinear
    Optical Properties of Schiff-Base-Containing Conductive Polymer Films
    Electro-deposited in Microgravity, Adv. Mater. 20 (2008) 2280–2284.
    </p>
    <p class="paragraph">
    [50] D. Runsewe, T. Betancourt, J.&nbsp;A. Irvin, Biomedical Application
    of Electroactive Polymers in Electrochemical Sensors: A Review,
    Materials 12 (2019) 2629.
    </p>
    <p class="paragraph">
    [51] Y. Yan, Y. Jiang, E.&nbsp;L.&nbsp;L. Ng, Y. Zhang, C. Owh, F. Wang,
    Q. Song, T. Feng, B. Zhang, P. Li, X.&nbsp;J. Loh, S.&nbsp;Y. Chan,
    B.&nbsp;Q.&nbsp;Y. Chan, Progress and opportunities in additive
    manufacturing of electrically conductive polymer composites, Mater.
    Today Adv. 17 (2023) 100333.
    </p>
    <p class="paragraph">
    [52] Y. Arteshi, A. Aghanejad, S. Davaran, Y. Omidi, Biocompatible and
    electroconductive polyaniline-based biomaterials for electrical
    stimulation, Eur. Polym. J. 108 (2018) 150–170.
    </p>
    <p class="paragraph">
    [53] E. Łyszczarz, W. Brniak, J. Szafraniec-Szczęsny, T.&nbsp;M. Majka,
    D. Majda, M. Zych, K. Pielichowski, R. Jachowicz, The Impact of the
    Preparation Method on the Properties of Orodispersible Films with
    Aripiprazole: Electrospinning vs. Casting and 3D Printing Methods,
    Pharmaceutics 13 (2021) 1122.
    </p>
    <p class="paragraph">
    [54] M. Bhandari, D.&nbsp;P. Kaur, S. Raj, T. Yadav, M.&nbsp;A.&nbsp;S.
    Abourehab, M.&nbsp;S. Alam, Electrically Conducting Smart Biodegradable
    Polymers and Their Applications, in: G.&nbsp;A.M. Ali,
    A.&nbsp;S.&nbsp;H. Makhlouf (Eds.), Handbook of Biodegradable Materials,
    Springer, Cham. (2023) 391–413.
    </p>
    <p class="paragraph">
    [55] S. Lee, B. Ozlu, T. Eom, D.C. Martin, B.&nbsp;S. Shim, Electrically
    conducting polymers for bio-interfacing electronics: From neural and
    cardiac interfaces to bone and artificial tissue biomaterials, Biosens.
    Bioelectron. 170 (2020) 112620.
    </p>
    <p class="paragraph">
    [56] A. Markov, R. Wördenweber, L. Ichkitidze, A. Gerasimenko, U.
    Kurilova, I. Suetina, M. Mezentseva, A. Offenhäusser, D. Telyshev,
    Biocompatible SWCNT Conductive Composites for Biomedical Applications,
    Nanomaterials 10 (2020) 2492.
    </p>
    <p class="paragraph">
    [57] S.&nbsp;C. Wang, K.&nbsp;S. Chang, C.&nbsp;J. Yuan, Enhancement of
    electrochemical properties of screen-printed carbon electrodes by oxygen
    plasma treatment, Electrochim. Acta 54 (2009) 4937–4943.
    </p>
    <p class="paragraph">
    [58] R. Ghobeira, C. Philips, H. Declercq, P. Cools, N. De Geyter, R.
    Cornelissen, R. Morent, Effects of different sterilization methods on
    the physicochemical and bioresponsive properties of plasma-treated
    polycaprolactone films, Biomed.Mater. 12 (2017) 015017.
    </p>
    <p class="paragraph">
    [59] A.&nbsp;C. da Silva, S.&nbsp;I. Córdoba de Torresi, Advances in
    Conducting, Biodegradable and Biocompatible Copolymers for Biomedical
    Applications, Front. Mater. 6 (2019).
    </p>
    <p class="paragraph">
    [60] S. Cheruthazhekatt, M. Černák, P. Slavíček, J. Havel, Gas plasmas
    and plasma modified materials in medicine, J. Appl. Biomed. 8 (2010)
    55–66.
    </p>
    <p class="paragraph">
    [61] M.&nbsp;N. Barshutina, V.&nbsp;S. Volkov, A.&nbsp;V. Arsenin,
    D.&nbsp;I. Yakubovsky, A.&nbsp;V. Melezhik, A.&nbsp;N. Blokhin,
    A.&nbsp;G. Tkachev, A.&nbsp;V. Lopachev, V.&nbsp;A. Kondrashov,
    Biocompatible, Electroconductive, and Highly Stretchable Hybrid Silicone
    Composites Based on Few-Layer Graphene and CNTs, Nanomaterials (Basel)
    11 (2021).
    </p>
    <p class="paragraph">
    [62] S. Lyu, D. Untereker, Degradability of Polymers for Implantable
    Biomedical Devices, Int. J. Mol. Sci. 10 (2009) 4033–4065.
    </p>
    <p class="paragraph">
    [63] N.&nbsp;J. Lores, X. Hung, M.&nbsp;H. Talou, G.&nbsp;A. Abraham,
    P.&nbsp;C. Caracciolo, Novel three-dimensional printing of poly(ester
    urethane) scaffolds for biomedical applications, Polym. Adv.Technol. 32
    (2021) 3309–3321.
    </p>
    <p class="paragraph">
    [64] M. Pajic, J. Zhihao, A. Connolly, R. Mangharam, A Framework for
    Validation of Implantable Medical Devices, Real-Time and Embedded
    Systems Lab (mLAB) (2010).
    </p>
    <p class="paragraph">
    [65] A.&nbsp;B. Amar, A.&nbsp;B. Kouki, H. Cao, Power Approaches for
    Implantable Medical Devices, Sensors 15 (2015) 28889–28914.
    </p>
    <p class="paragraph">
    [66] M.&nbsp;M.&nbsp;H. Shuvo, T. Titirsha, N. Amin, S.&nbsp;K. Islam,
    Energy Harvesting in Implantable and Wearable Medical Devices for
    Enduring Precision Healthcare, Energies 15 (2022) 7495.
    </p>
    <p class="paragraph">
    [67] H. Esmaeili, A. Patino-Guerrero, M. Hasany, M.&nbsp;O. Ansari, A.
    Memic, A. Dolatshahi-Pirouz, M. Nikkhah, Electroconductive biomaterials
    for cardiac tissue engineering, Acta Biomater. 139 (2022) 118–140.
    </p>
    <p class="paragraph">
    [68] B. Podsiadły, P. Walter, M. Kamiński, A. Skalski, M. Słoma,
    Electrically Conductive Nanocomposite Fibers for Flexible and Structural
    Electronics, Appl. Sci. 12 (2022) 941.
    </p>
    <p class="paragraph">
    [69] S.&nbsp;M.&nbsp;A. Mokhtar, E. Alvarez de Eulate, M. Yamada,
    T.&nbsp;W. Prow, D.&nbsp;R. Evans, Conducting polymers in wearable
    devices, Med. Devices Sens. 4 (2021) e10160.
    </p>
    <p class="paragraph">
    [70] S. Peng, Y. Yu, S. Wu, C.-H. Wang, Conductive Polymer
    Nanocomposites for Stretchable Electronics: Material Selection, Design,
    and Applications, ACS Appl. Mater. Interfaces 13 (2021) 43831–43854.
    </p>
    <p class="paragraph">
    [71] C. Park, M.&nbsp;S. Kim, H.&nbsp;H. Kim, S.-H. Sunwoo, D.&nbsp;J.
    Jung, M.&nbsp;K. Choi, D.-H. Kim, Stretchable conductive nanocomposites
    and their applications in wearable devices, Appl. Phys. Rev. 9 (2022).
    </p>
    <p class="paragraph">
    [72] C.&nbsp;A.&nbsp;R. Chapman, E.&nbsp;A. Cuttaz, J.&nbsp;A. Goding,
    R.&nbsp;A. Green, Actively controlled local drug delivery using
    conductive polymer-based devices, Appl. Phys. Lett. 116 (2020).
    </p>
    <p class="paragraph">
    [73] S. Adepu, S. Ramakrishna, Controlled Drug Delivery Systems: Current
    Status and Future Directions, Molecules 26 (2021) 5905.
    </p>
    <p class="paragraph">
    [74] M.&nbsp;D. Ashton, P.&nbsp;A. Cooper, S. Municoy, M.&nbsp;F.
    Desimone, D. Cheneler, S.&nbsp;D. Shnyder, J.&nbsp;G. Hardy, Controlled
    Bioactive Delivery Using Degradable Electroactive Polymers,
    Biomacromolecules 23 (2022) 3031–3040.
    </p>
    <p class="paragraph">
    [75] M.-L. Laracuente, M.&nbsp;H. Yu, K.&nbsp;J. McHugh, Zero-order drug
    delivery: State of the art and future prospects, J. Control. Release 327
    (2020) 834–856.
    </p>
    <p class="paragraph">
    [76] S. Gungordu Er, A. Kelly, S.&nbsp;B.&nbsp;W. Jayasuriya, M.
    Edirisinghe, Nanofiber Based on Electrically Conductive Materials for
    Biosensor Applications, Biomed. Mater. Devices (2022)
    <a href="https://doi.org/10.1007/s44174-022-00050-z" rel="" target="blank">https://doi.org/10.1007/s44174–022–00050-z</a>.
    </p>
