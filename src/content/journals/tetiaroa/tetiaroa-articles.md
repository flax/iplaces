---
title: "The articles"
pagination: 
  data: journalsdata["tetiaroa-ecostation-unfiltered"]
  size: 1
  alias: "article"
journalid: "tetiaroa-ecostation"
permalink: "/{{ journalid }}/{{ article.volume }}/{{ article.issue }}/{{article.title | slugify}}-{{article.DOI}}"
layout: "/journals/journal-article-single.njk"
---


