---
title: "About"
layout: "journals/journal-singlepage.njk"
cover:
  image: "about.jpg"
  alttext: "A color photo of the Tetiaroa atol"
---

## About Tetiaroa Ecostation

**Tetiaroa journal** is a platform that integrates tools focused on the critical early ‘field’ phases of research projects that sample and digitize social-ecological systems. It aims to serve and empower places, giving local stewards (and ordinary citizens) greater agency over the information generated in and about the people, culture and nature that constitute their place. Our approach is to innovate and disrupt the entire publishing model while still utilizing its key platforms, tools, and incentive structures.
