---
title: "Gump Station articles"
pagination:
  data: journalsdata["gump-station-unfiltered"]
  size: 1
  alias: "article"
journalid: "gump-station"
permalink: "/gump-station/{{ article.volume }}/{{ article.issue }}/{{article.title | slugify}}-{{article.DOI}}"
layout: "/journals/journal-article-single.njk"
---
