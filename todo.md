


home Tenancy
  <!-- tenancy for the home page should have those files -->
  /                      (homepage)
  /about/                (about page)
  /team/                 (team page for the whole site) 
  /journals/             (list of the journals)


journals pages
  <!-- tenancy for the journal should have those files -->
  /journals/{{journal.name}}/home/
  /journals/{{journal.name}}/team/
  /journals/{{journal.name}}/volumes/  # page that show all the previous volumes and issues
  /journals/{{journal.name}}/volumes/{{volume}}/{{issue}}



the journals are defined in the journals.json file and the meta data are there.


deconstruct volumes/issues articlesz 

```
function query (data) {

const newdata = []

for (let i = 0; i < data.length; i++) {
for (let j = 0; j < data[i].issues.length; j++) {
for (let k = 0; k < data[i].issues[j].articles.length; k++) {
newdata.push(data[i].issues[j].articles[k])
}
}
}

return newdata
} 
``` 

