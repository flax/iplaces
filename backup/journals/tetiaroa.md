---
title: "Tetiaora"
journalid: "tetiaroa-ecostation"
baseline: the journal one
permalink: "/tetiaroa/"
menutitle: journalpage
layout: "journals/journal-single.njk"
colors:
  background: "#EFFFF5"
  # background: "#E8FFF1"
  # background: "#E2FFD8"
  main: "#4B8562"
  backgrounddark: "#88BE9D"
image:
  journalimage: "img2.jpg"
  alttext: the cover for tetiaroa one
cover:
  filename: "cover1.jpg"
  alt: "tetiaroa cover of the journal"
latestissue:
  volume: "IV"
  issue: 2
  title: "Title of the issue as the issue has a title"
  cover: "cover1.jpg"
---

**Tetiaroa journal** is a platform that integrates tools focused on the critical early ‘field’ phases of research projects that sample and digitize social-ecological systems. It aims to serve and empower places, giving local stewards (and ordinary citizens) greater agency over the information generated in and about the people, culture and nature that constitute their place. Our approach is to innovate and disrupt the entire publishing model while still utilizing its key platforms, tools, and incentive structures.
