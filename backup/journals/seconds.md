---
title: "Seconds"
journalid: "Seconds"
baseline: the journal bis
colors: 
  # background: "#C7E4EB"
  background: "#D9F8FF"
  main: "#008CAC"
cover: 
  filename: "cover2.jpg"
  alt: "tetiaroa cover of the journal" 
latestissue:
  volume: "III"
  issue: 1
  title: "Title of the issue as the issue has a title"
  cover: "cover1.jpg"
articles:
  - title: "title of the article"
    dates:
      published: 2012/02/28
    abstract: |
      "Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat."
    authors:
      - author 1
      - author 2
      - author 3
    affiliations:
      - affiliation 1
      - affiliation 2
      - affiliation 3
---

__This journal__ lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet. Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident. Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.
