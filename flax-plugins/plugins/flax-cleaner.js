const path = require("path");
const htmlmin = require("html-minifier");

const prettier = require("prettier");

module.exports = (eleventyConfig) => {
  // onlyonbuild please!

  eleventyConfig.addTransform("prettier", function(content, outputPath) {
    if (outputPath == false) return content;
    if (!content) return;
    const extname = path.extname(outputPath);
    switch (extname) {
      case "json":
        // Strip leading period from extension and use as the Prettier parser.
        const parser = extname.replace(/^./, "");
        return prettier.format(content, { parser });
      default:
        return content;
    }
  });

  // onlyonbuild please!
  eleventyConfig.addTransform("htmlmin", function(content) {
    if (this.page.outputPath == false) return content;
    if (!content) return;
    if (this.page.outputPath && this.page.outputPath.endsWith(".html")) {
      let minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true,
      });
      return minified;
    }

    return content;
  });
};
