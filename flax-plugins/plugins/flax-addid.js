const slugify = require("slugify");

const { parseHTML } = require("linkedom");

module.exports = function(eleventyConfig, options) {
  eleventyConfig.addFilter("addID", function(data, page, count = true) {
    
    if (!data) return "";

    const { document } = parseHTML(data);

    document.querySelectorAll("h2, h3, h4, h5, h6").forEach((el, index) => {
      if (el.id) return;
      el.id = `${slugify(el.textContent, {
        remove: /[*+~.()'"!:@,]/g,
      })}_${index}`;
    });
    return document.toString();
  });
};
