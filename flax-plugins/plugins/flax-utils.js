// multiple needed filters
//
module.exports = function(eleventyConfig) {
  eleventyConfig.addFilter("not", function(array, key, value) {
    return array.filter((el) => {
      return el[key] != value;
    });
  });

  eleventyConfig.addFilter("with", function(array, key, value) {
    const filtered = array.filter((el) => {
      return el.data[key] == value;
    });
    return filtered;
  });

  // width
  eleventyConfig.addFilter("withData", function(array, key, value) {
    const filtered = array.filter((el) => {
      return el[key] == value;
    });
    return filtered;
  });
  eleventyConfig.addFilter("notcontains", function(array, key, value) {
    return array.filter((el) => {
      return !el[key].includes(value);
    });
  });

  eleventyConfig.addFilter("contains", function(array, key, value) {
    return array.filter((el) => {
      return el[key].includes(value);
    });
  });

  eleventyConfig.addFilter(
    "limitString",
    function(data, limit = 45, char = "_") {
      
      if(!data) return ""

      const wordcount = data.split(" ")

      if(wordcount.length < limit) return data 
      wordcount.length=limit
      return `${wordcount.join(" ")}${char}`;
    },
  );
};
