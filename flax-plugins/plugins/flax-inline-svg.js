// based on Brette DeWoody’s post, thanks infinitely
// https://medium.com/@brettdewoody/inlining-svgs-in-eleventy-cffb1114e7b
//

const Image = require("@11ty/eleventy-img");

const { parseHTML } = require("linkedom");
const fs = require("fs");

module.exports = async function (eleventyConfig, options = {}) {
  eleventyConfig.addAsyncShortcode(
    "rendersvg",
    async (src, alt, sizes) => {
      let metadata = await Image(src, {
        formats: ["svg"],
        dryRun: true,
      });
      if(!metadata) return ""
      return metadata.svg[0].buffer.toString();
    },
  );
  // eleventyConfig.addTransform("inlinesvg", async function(content) {
  // })
};
