// based on Gregory’s Cadars css only slide show system, thanks infinitely
// https://codepen.io/cadars/pen/GRMopvj
// but it changed quite a lot (still, Cadars is amazing, get him to work with you):D

const Image = require("@11ty/eleventy-img");
const fs = require("fs");
const slugify = require("slugify");

module.exports = function (eleventyConfig) {
  // create the image for the block
  eleventyConfig.addAsyncShortcode("flaxSliderImage", async (src, alt) => {
    //check if the url is external
    let imgSrcUrl = !src.startsWith("http") ? `./static/images/${src}` : src;

    // lets create the images
    let metadata = await Image(imgSrcUrl, {
      widths: [600],
      formats: ["jpeg"],
      outputDir: "./public/images",
      urlPath: "/images/",
    });

    let data = metadata.jpeg[metadata.jpeg.length - 1];

    let output = ` 

<a id="#${slugify(src)}" href="#${slugify(src)}">
<figure id="${slugify(src)}">
        <img src="${data.url}" width="${data.width}" height="${
          data.height
        }" alt="${alt}" loading="lazy" decoding="async">
      </figure>
</a>
`;

    return output;
  });

  eleventyConfig.addPairedShortcode("flaxSlider", (content) => {
    let output = `<div class="flax-slideshow">${content}<p class="empty"><p></div>`;
    return output;
  });
};
