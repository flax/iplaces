// add filter to have markdownify and inline and set the markdown language for eleventy

const markdownIt = require("markdown-it");
const markdownItPandoc = require("markdown-it-pandoc");
const markdownItAnchor = require("markdown-it-anchor");

let options = {
  html: true, // Enable HTML tags in source
  linkify: true,
  breaks: true,
};


// configure the library with options
let md = markdownIt(options).use(markdownItPandoc).use(markdownItAnchor);

module.exports = function(eleventyConfig, options) {

  eleventyConfig.setLibrary("md", markdownIt(options));

  eleventyConfig.addFilter("markdownify", function(value) {
    if (!value) return "";
    return md.render(value);
  });

  eleventyConfig.addFilter("markdownifyInline", function(value) {
    if (!value) return "";
    return md.renderInline(value);
  });
};



